'''
Parse MIMIC-III note events text into a MongoDB collection.
'''

from csv import DictReader

from util.mydbs import MONGO_CLIENT


def run():
    '''
    Run program.    
    '''
    pkgs = []
    coll = MONGO_CLIENT['penq']['mimic3']
    path = '/home/jessewang/Downloads/NOTEEVENTS.csv'
    with open(path, encoding='utf8') as f:
        reader = DictReader(f, delimiter=',')
        for row in reader:
            pkgs.append({'note': row['TEXT']})
            if len(pkgs) == 1000:
                coll.insert_many(pkgs)
                pkgs.clear()
        coll.insert_many(pkgs)


if __name__ == '__main__':
    run()
