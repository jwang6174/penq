'''
The "Bronze Age" of machine learning, e.g. naive Bayes, SVM, and
logistic regression.
'''

import json
import os

from pickle import dump
from random import shuffle

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV, PredefinedSplit
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

from training.classify.preprocess import preprocess
from util.pathutil import LOGS_DIR, MODELS_DIR, DUMPS_DIR
from util.mydbs import MONGO_CLIENT
from util.timeutil import timelog

# Results path.
RESULTS_PATH = os.path.join(LOGS_DIR, 'bronze.log')


def _get_training_samples():
    '''
    Get training and validation samples.

    Returns:
    (list: str) Randomly shuffled sentences.
    (list: str) Randomly shuffled categories, aligned with their
    respective sentences.
    (list: int) Gives the test fold of each sample.
    '''
    timelog('Loading training samples')

    samples = []
    test_fold = []
    for category in ['everyday', 'medical']:
        for fold in ['training', 'validation']:
            dump_name = f'sentences_{category}_{fold}.json'
            dump_path = os.path.join(DUMPS_DIR, dump_name)
            timelog(f'Loading {dump_name}')
            with open(dump_path) as f:
                for doc in json.load(f):
                    sentence = preprocess(doc['sentence'])
                    if sentence:
                        if fold == 'training':
                            code = -1
                        else:
                            code = 0
                        samples.append((sentence, category, code))

    timelog('Shuffling training samples')
    shuffle(samples)

    X, y, test_fold = [], [], []
    for (sentence, category, code) in samples:
        X.append(sentence)
        y.append(category)
        test_fold.append(code)

    return (X, y), test_fold


def _get_test_samples():
    '''
    Get test samples.

    Returns:
    (list: str) Sentences.
    (list: str) Categories.
    '''
    timelog('Loading test samples')
    X = []
    y = []
    for category in ['everyday', 'medical']:
        dump_name = f'sentences_{category}_test.json'
        dump_path = os.path.join(DUMPS_DIR, dump_name)
        timelog(f'Loading {dump_name}')
        with open(dump_path) as f:
            for doc in json.load(f):
                sentence = preprocess(doc['sentence'])
                if sentence:
                    X.append(sentence)
                    y.append(category)
    return X, y


def _log(model_name, gs_clf, parameters, y_test, predictions):
    '''
    Log results of best classifier identified with grid search.

    (str) model_name -- Model name.
    (obj) gs_clf -- Grid search classifier object.
    (dict) parameters -- Grid search parameters.
    (list: str) y_test -- Test categories.
    (list: str) predictions -- Classifier predictions.
    '''
    with open(RESULTS_PATH, 'a') as f:
        report = classification_report(y_test, predictions, digits=5)
        f.write(f'{model_name}\n')
        f.write(f'{report}\n')
        for name in sorted(parameters.keys()):
            f.write(f'{name}: {gs_clf.best_params_[name]}\n')
        f.write('\n')


def _try(model_name, classifier, parameters, test_fold, samples_train, samples_test):
    '''
    Fit classifier with grid search, evaluate, and store.

    Args:
    (str) model_name -- Model name.

    (obj) classifier -- Unfitted Pipeline object.

    (dict) parameters -- Grid search parameters.

    (list: int) test_fold -- Gives the test set fold of sample i, 0  for all
    samples that are part of the validation set and -1 for training
    samples.

    (list: str, str) samples_train -- Training samples.

    (list: str, str) samples_test-- Test samples.
    '''
    timelog(f'Trying {model_name}')

    # Split samples.
    X_train, y_train = samples_train
    X_test, y_test = samples_test

    # Tell GridSearchCV which samples are part of the training set and
    # which are part of the validation set.
    ps = PredefinedSplit(test_fold=test_fold)

    # Set up grid search classifier.
    gs = GridSearchCV(classifier, parameters, cv=ps, n_jobs=1)
    gs_clf = gs.fit(X_train, y_train)

    # Make predictions.
    predictions = gs_clf.predict(X_test)

    # Write classifier performance results.
    _log(model_name, gs_clf, parameters, y_test, predictions)

    # Save classifier.
    savename = model_name.replace(' ', '_').lower() + '.model'
    savepath = os.path.join(MODELS_DIR, savename)
    dump(gs_clf, open(savepath, 'wb'))


def _try_NB(test_fold, samples_train, samples_test):
    '''
    Try naive Bayes.

    Args:
    (list: int) Gives the fold for each training sample.
    (list: str, str) samples_train -- Training samples.
    (list: str, str) samples_test -- Test samples.
    '''
    classifier = Pipeline([
        ('count', CountVectorizer()),
        ('tfidf', TfidfTransformer(sublinear_tf=True)),
        ('clf', MultinomialNB())
    ])
    parameters = {}
    _try('Naive Bayes', classifier, parameters, test_fold, samples_train, samples_test)


def _try_LR(test_fold, samples_train, samples_test):
    '''
    Try logistic regression.

    Args:
    (list: int) Gives the fold for each training sample.
    (list: str, str) samples_train -- Training samples.
    (list: str, str) samples_test -- Test samples.
    '''
    classifier = Pipeline([
        ('count', CountVectorizer()),
        ('tfidf', TfidfTransformer(sublinear_tf=True)),
        ('clf', LogisticRegression())
    ])
    parameters = {
        'clf__dual': [False],
        'clf__C': [0.1, 1.0, 10.0],
        'clf__solver': ['saga'],
    }
    _try('Logistic Regression', classifier, parameters, test_fold, samples_train, samples_test)


def _try_SVM(test_fold, samples_train, samples_test):
    '''
    Try support vector machine.

    Args:
    (list: int) Gives the fold for each training sample.
    (list: str, str) samples_train -- Training samples.
    (list: str, str) samples_test -- Test samples.
    '''
    classifier = Pipeline([
        ('count', CountVectorizer()),
        ('tfidf', TfidfTransformer(sublinear_tf=True)),
        ('clf', LinearSVC())
    ])
    parameters = {
        'clf__dual': [False],
        'clf__C': [0.1, 1.0, 10.0],
        'clf__max_iter': [3000]
    }
    _try('SVM', classifier, parameters, test_fold, samples_train, samples_test)


def run():
    '''
    Run program.
    '''

    timelog('Running bronze.py')

    samples_train, test_fold = _get_training_samples()
    samples_valid = _get_test_samples()

    _try_NB(test_fold, samples_train, samples_valid)

    _try_LR(test_fold, samples_train, samples_valid)

    _try_SVM(test_fold, samples_train, samples_valid)

    timelog('All done ^_^')
