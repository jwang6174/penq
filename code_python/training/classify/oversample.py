'''
Oversample the medical sentence class to approximately match the
everyday sentence class.
'''

from random import uniform

from util.timeutil import timelog
from util.mydbs import MONGO_CLIENT


def run():
    '''
    Run program.
    '''
    examples = []
    for dataset in ['validation', 'test', 'training']:
        collection = MONGO_CLIENT['penq'][f'sentences.medical.{dataset}']
        for doc in collection.find()[0:collection.count()]:
            examples.append({'sentence': doc['sentence'], 'oversample': True})
            if uniform(0, 1) <= 0.22:
                examples.append({'sentence': doc['sentence'], 'oversample': True})
            if len(examples) > 100_000:
                collection.insert_many(examples)
                examples.clear()
        collection.insert_many(examples)
        examples.clear()
