'''
Perform text pre-processing.
'''

from random import random


def _add_padding(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with extra space at the beginning and end.
    '''
    return ' ' + s +  ' '


def _sub_slashes(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with forward slashes replaced by space.
    '''
    return s.replace('/', ' ')


def _del_artifacts(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with '?????' replaced by a whitespace. This token is
    present in some MIMIC-III notes.
    '''
    return s.replace('?????', ' ')


def _del_hyphens(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with hyphens replaced by whitespace.
    '''
    return s.replace('-', ' ')


def _sub_abbreviations(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with some abbreviations replaced by the full spelling.
    '''
    pos = ' positive '
    neg = ' negative '
    s = s.replace('+', pos)
    s = s.replace('-', neg)
    s = s.replace(' plus ', pos)
    s = s.replace(' minus ', neg)
    s = s.replace(' reg ', ' regular ')
    s = s.replace(' nl ', ' normal ')
    return s


def _sub_digits(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with spaces before and after digits.
    '''
    for char in '0123456789':
        s = s.replace(char, ' ' + char + ' ')
    return s


def _del_extra_spaces(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String without extra whitespaces.
    '''
    return ' '.join(s.split())


def _del_non_alpha_nums(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) String with only alphanumeric characters and whitespaces.
    '''
    t = ''
    for char in s:
        if char.isalnum() or char.isspace():
            t += char
    return t


def preprocess(s):
    '''
    Args:
    (str) s -- Some string.

    Returns:
    (str) Pre-processed string.
    '''
    s = s.lower()
    s = _del_extra_spaces(s)
    s = _add_padding(s)
    s = _sub_slashes(s)
    s = _sub_digits(s)
    s = _sub_abbreviations(s)
    s = _del_artifacts(s)
    s = _del_hyphens(s)
    s = _del_non_alpha_nums(s)
    s = _del_extra_spaces(s)
    return s
