'''
Collect everyday and medical sentences.
'''

import sys

import spacy
from numpy.random import choice

from util.mydbs import MONGO_CLIENT
from util.timeutil import timelog

# Set up SpaCy with unnecessary components disabled.
NLP = spacy.load('en')
NLP.disable_pipes('tagger', 'parser', 'ner')
NLP.add_pipe(NLP.create_pipe('sentencizer'))


def _insert(texts, registry, category):
    '''
    Insert samples into the appropriate MongoDB collections.

    Args:
    (list: str) texts -- MongoDB doc texts.
    (dict: list) registry -- Dataset and samples.
    (str) category -- Sentence category i.e. everday or medical.
    '''
    for spacy_doc in list(NLP.pipe(texts)):
        for sent in spacy_doc.sents:
            dataset = choice(['training', 'validation', 'test'], p=[0.8, 0.1, 0.1])
            registry.setdefault(dataset, []).append({'sentence': sent.text})

    for dataset, samples in registry.items():
        target_name = f'sentences.{category}.{dataset}'
        MONGO_CLIENT['penq'][target_name].insert_many(samples)
        registry[dataset].clear()

    texts.clear()


def run(source_name, source_field, category):
    '''
    Collect sentences.

    Args:
    (str) source_name -- Source collection name.
    (str) source_field -- Field name for source collection.
    (str) category -- Sentence category i.e. everday or medical.
    '''
    count = 0
    texts = []
    registry = {}

    for mongo_doc in MONGO_CLIENT['penq'][source_name].find():
        count += 1
        if mongo_doc[source_field]:
            texts.append(mongo_doc[source_field])
        if count % 100 == 0:
            _insert(texts, registry, category)
            timelog(f'{source_name}: {count}')

    _insert(texts, registry, category)
    timelog(f'{source_name}: {count}')
