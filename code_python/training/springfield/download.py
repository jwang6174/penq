'''
Download Springfield scripts for movies and TV episodes into a MongoDB
collection.

URL: https://www.springfieldspringfield.co.uk
'''

import requests
import string

from bs4 import BeautifulSoup
from pymongo import InsertOne

from util.mydbs import MONGO_CLIENT


def _get_script_links(page_link):
    '''
    Args:
    (str) page_link -- Page link for alphabetical 

    Returns:
    (list: str) Script links.
    '''
    try:
        page = requests.get(page_link)
        soup = BeautifulSoup(page.text, 'html.parser')
        root = 'https://www.springfieldspringfield.co.uk'
        return [(root + a['href']) for a in soup.find_all(class_='script-list-item')]
    except:
        return []


def _get_page_links(order_link):
    '''
    Args:
    (str) order_link -- Alphabetical link for movie titles.

    Returns:
    (list: str) Page links for alphabetical link.
    '''
    try:
        links = []
        root = order_link + '&page='
        page = requests.get(order_link)
        soup = BeautifulSoup(page.text, 'html.parser')
        max_ = max([int(a.text) for a in soup.find(class_='pagination').find_all('a')])
        return [(root + str(i)) for i in range(1, max_ + 1)]
    except:
        return []


def _get_order_links(script_type):
    '''
    Args:
    (bool) script_type -- True for movies, false for TV series episodes.

    Returns:
    (list: str) The alphabetical links for movie titles.
    '''
    root = 'https://www.springfieldspringfield.co.uk/'
    if script_type:
        subroot = root + 'movie_scripts.php?order='
    else:
        subroot = root + 'tv_show_episode_scripts.php?order='
    try:
        return [(subroot + alpha) for alpha in _get_alphas()]
    except:
        return []


def _get_alphas():
    '''
    Returns:
    (list: str) A-Z and 0.
    '''
    alphas = list(string.ascii_uppercase)
    alphas.append('0')
    return alphas


def _get_script(script_link):
    '''
    Args:
    (str) script_link -- Script link.

    Returns:
    (str) Script.
    '''
    try:
        page = requests.get(script_link)
        soup = BeautifulSoup(page.text, 'html.parser')
        return (soup.find(class_='scrolling-script-container')
            .get_text(separator='\n', strip=True))
    except:
        return None


def _get_episode_links(script_link):
    '''
    Args:
    (str) script_link -- Script link.

    Returns:
    (list: str) Episode links.
    '''
    try:
        page = requests.get(script_link)
        soup = BeautifulSoup(page.text, 'html.parser')
        root = 'https://www.springfieldspringfield.co.uk/'
        return [(root + a['href']) for a in soup.find_all(class_='season-episode-title')]
    except:
        return []


def run():
    '''
    Run program.
    '''
    collection = MONGO_CLIENT['penq']['springfield']
    for script_type in [True, False]:
        for order_link in _get_order_links(script_type):
            for page_link in _get_page_links(order_link):
                for script_link in _get_script_links(page_link):
                    if script_type:
                        collection.insert_one({
                            'script': _get_script(script_link),
                            'script_link': script_link})
                        print(script_link)
                    else:
                        for episode_link in _get_episode_links(script_link):
                            collection.insert_one({
                                'script': _get_script(episode_link),
                                'script_link': episode_link})
                            print(episode_link)
