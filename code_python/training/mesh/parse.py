'''
Extract MeSH term and synonyms from ASCII source file.
'''


class MeshDB:
    '''
    MeshDoc objects from a MeSH ASCII source file.
    '''
    def __init__(self, filepath):
        '''
        Args:
        (str) filepath -- File path of MeSH ASCII source file.
        '''
        self.filepath = filepath
        self.text = self.open_text()
        self.docs = self.make_docs()

    def open_text(self):
        '''
        Returns:
        (str) Text from MeSH ASCII file.
        '''
        with open(self.filepath, 'r') as f:
            return f.read()

    def make_docs(self):
        '''
        Returns:
        (list: MeshDoc) Objects containing terms and their respective
        synonyms.
        '''
        blobs = self.text.split('\n\n')
        return [MeshDoc(b) for b in blobs]


class MeshDoc:
    '''
    MeSH term and its synonyms.
    '''
    def __init__(self, blob):
        '''
        Args:
        (str) blob -- Text chunk from MeSH ASCII file for a single MeSH
        term.
        '''
        self.blob = blob
        self.data = self.get_data()
        self.term = self.get_term()
        self.syns = self.get_syns()

    def get_data(self):
        '''
        Returns:
        (list: DataLine) Contents of each line in blob.
        '''
        data = []
        for line in self.blob.split('\n'):
            if DataLine.is_data_line(line):
                data.append(DataLine(line))
        return data

    def get_term(self):
        '''
        Returns:
        (str or None) Main MeSH term.
        '''
        for d in self.data:
            if d.key == 'MH' or d.key == 'NM':
                return d.val
        return None

    def get_syns(self):
        '''
        Returns:
        (list: str or None) Synonyms delimited by the pipe character.
        '''
        syns = []
        for d in self.data:
            if d.key == 'ENTRY' or d.key == 'PRINT ENTRY' or d.key == 'SY':
                syns.append(d.val.split('|')[0])
        return syns if syns else None


class DataLine:
    '''
    Contains key-value information for certain lines in MeSH ASCII
    file.

    Note:
    Before creating an instance of this class with a line of text, user
    needs to check that the line is valid with the static function
    is_data_line.
    '''

    delim = ' = '

    @staticmethod
    def is_data_line(text):
        '''
        Args:
        (str) text -- Line of text in MeSH ASCII file. This function
        checks if this string contains the ' = ' substring.

        Returns:
        (bool) True if text contains ' = ' substring, else false.
        '''
        return DataLine.delim in text

    def __init__(self, text):
        '''
        Args:
        (str) text -- Line of text in MeSH ASCII file. Assumed to have the
        ' = ' substring.
        '''
        self.text = self.modify(text)
        self.key = self.get_key()
        self.val = self.get_val()

    def modify(self, text):
        '''
        Args:
        (str) text -- Line of text in MeSH ASCII file. Assumed to have
        the ' = ' substring.

        Returns:
        (str) Text without '\n' and '\r' characters.
        '''
        return text.replace('\n', '').replace('\r', '')

    def get_key(self):
        '''
        Returns:
        (str) Data type e.g. 'ENTRY', 'PRINT ENTRY', 'SY'; the
        left-hand side of the ' = ' substring.
        '''
        return self.text.split(DataLine.delim)[0]

    def get_val(self):
        '''
        Return:
        (str) Data value; the right-hand of the ' = ' substring.
        '''
        return self.text.split(DataLine.delim)[1]
