'''
Add MeSH term and synonyms to MongoDB.

https://www.nlm.nih.gov/mesh/filelist.html
'''
import os

from pymongo import MongoClient

from data.mesh.parse import MeshDB
from util.mydbs import MONGO_CLIENT
from util.timeutil import timelog
from util.pathutil import MESH_DIR

# MeSH ASCII file paths
PATHS = [
    os.path.join(MESH_DIR, 'c2020.bin'),
    os.path.join(MESH_DIR, 'd2020.bin')
]

# MongoDB collection
MESH_DB = MONGO_CLIENT['penq']['mesh']


def run():
    '''
    Add contents of all MeSH ASCII files to MongoDB.
    '''
    timelog('mesh crawl starting')

    updates = []
    for path in PATHS:
        for doc in MeshDB(path).docs:
            if doc.term is not None:
                updates.append({'term': doc.term, 'synonyms': doc.syns})

    MESH_DB.insert_many(updates)

    timelog('mesh crawl complete')
