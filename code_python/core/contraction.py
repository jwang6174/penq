'''
Expand contractions.

Reference: https://dictionary.cambridge.org/us/grammar/british-grammar/writing/contractions.
'''

import re
import spacy
import string

from core.textutil import NLP, cap_first, first_letter_is_cap


def expand(text):
    '''
    Args:
    (str) text -- Expand contractions in this text.

    Returns:
    (str) Text with expanded contractions.
    '''
    text = _expand_misc(text)
    text = _expand_rule(text)
    return text


def _expand_misc(text):
    '''
    Args:
    (str) text -- Expand miscellaneous contractions in text.

    Returns:
    (str) Text with select contractions expanded.
    '''
    misc = {
        "'cause": "because",
        "'coz": "because",
        "'cuz": "because",
        "g'day": "good day",
        "wanna": "want to",
        "we'd've": "we would have",
        "y'all": "you all",
        "can't": "cannot",
        "won't": "will not",
        'sorta': 'sort of',
        'outta': 'out of',
        'kinda': 'kind of',
        'gotta': 'got to',
        'gonna': 'going to',
        'wanna': 'want to',
        'gimme': 'give me',
        'hafta': 'have to',
        'hasta': 'has to',
        'donno': 'do not know',
        'dunno': 'do not know',
        'coulda': 'could have',
        'lemme': 'let me',
        'gotcha': 'got you',
        'betcha': 'bet you',
        'shoulda': 'should have',
        'woulda': 'would have',
        'mighta': 'might have',
        'musta': 'must have',
        'couldna': 'could not have',
        'shouldna': 'should not have',
        'wouldna': 'would not have',
        'cuppa': 'cup of',
        'a lotta': 'a lot of',
        'oughta': 'ought to',
        'useta': 'used to'
    }
    for abbr, full in misc.items():
        matches = re.findall(abbr, text, re.IGNORECASE)
        for match in matches:
            if first_letter_is_cap(match):
                text = text.replace(match, cap_first(full))
            else:
                text = text.replace(match, full)
    return text


def _expand_rule(text):
    '''
    Args:
    (str) text -- Expand contractions in this text using rules.

    Returns:
    (str) Text with rule-based expansions.
    '''
    mod = ''
    doc = NLP(text)
    for token in doc:
        if "'" in token.text:
            sub = _replace(token)
            if sub:
                mod += ' ' + sub
            else:
                mod += token.text
            if token.text_with_ws.endswith(' '):
                mod += ' '
        else:
            mod += token.text_with_ws
    return mod.strip()


def _replace(token):
    '''
    Args:
    (obj) token -- SpaCy token object.

    Returns:
    (str or None) Expanded form if contraction, else token text.
    '''
    if token.text == "'m":
        return 'am'
    if token.text == "'re":
        return 'are'
    if token.text == "'s" and token.tag_ != 'POS':
        return _get_is_or_has(token)
    if token.text == "'ve":
        return 'have'
    if token.text == "'ll":
        return 'will'
    if token.text == "'d":
        return _get_had_or_would(token)
    if token.text == "n't":
        return 'not'
    return None


def _get_is_or_has(token):
    '''
    For the 's contraction.

    Args:
    (obj) token -- SpaCy token object.

    Returns:
    (str) "is" or "has", depending on the part-of-speech of the main
    verb.
    '''
    assert token.text == "'s"
    if token.head.tag_ == 'VBG':
        return 'is'
    if token.head.tag_ == 'VBN':
        return 'has'
    return 'is'


def _get_had_or_would(token):
    '''
    For the 'd contraction.

    Args:
    (obj) token -- SpaCy token object.

    Returns:
    (str) "had" or "would", depending on the part-of-speech of the
    main verb.
    '''
    assert token.text == "'d"
    if token.head.tag_ == 'VBN':
        return 'had'
    if token.head.tag_ == 'VB':
        return 'would'
    return 'had'
