'''
Helper module for working with text.
'''

import re
import string

import spacy
from spacy.tokens import Doc, Token
from benepar.spacy_plugin import BeneparComponent
from text2digits import text2digits

# SpaCy model.
NLP = spacy.load('en')
NLP.add_pipe(BeneparComponent('benepar_en2'))

# Set custom token attribute 'delete' to false. Used to mark tokens
# that should be deleted.
Token.set_extension('delete', default=False)

# Medical sound-a-likes.
_MEDICAL_SOUNDALIKES = {
    'breweries': 'bruits',
    'brewery': 'bruit',
    'by lateral': 'bilateral',
    'by laterally': 'bilaterally',
    'bye lateral': 'bilateral',
    'bye laterally': 'bilaterally',
    'in tag': 'intact',
    'and tag': 'intact',
    'and tact': 'intact',
    'in tact': 'intact'
}

# Text2digits object.
_T2D = text2digits.Text2Digits()


def get_medical_soundalike(text):
    '''
    Args:
    (str) text -- Some text to see if it has a medical sound-alike.

    Returns:
    (str) The text's medical sound-alike, if it has one, with the
    corresponding first-letter capitalization and ending punctuation.
    If there is no sound-alike, returns the text that was passed in.
    '''
    norm = normalize(text)
    alike = _MEDICAL_SOUNDALIKES.get(norm, text)
    if alike != text:
        if text[0].isupper():
            alike = cap_first(alike)
        if text[-1] in string.punctuation:
            alike += text[-1]
        return alike
    return text


def replace_medical_soundalikes(sentence):
    '''
    Args:
    (str) sentence -- Replace medical soundalikes in this sentence.

    Returns:
    (str) Sentence with converted soundalikes.
    '''
    for k, v in _MEDICAL_SOUNDALIKES.items():
        sentence = re.sub(k, v, sentence, re.IGNORECASE)
    sentence = sentence.strip()
    sentence = cap_first(sentence)
    sentence = add_period(sentence)
    return sentence


def add_period(text):
    '''
    Args:
    (str) text -- Add period to end of this text, if necessary.

    Returns:
    (str) Text with period at the end.
    '''
    text = text.strip()
    if text:
        if text[-1] in string.punctuation and text[-1] != '.':
            return text[:-1] + '.'
        elif text[-1] not in string.punctuation:
            return text + '.'
    return text


def cap_first(text):
    '''
    Args:
    (str) text -- Capitalize first letter of this text.

    Returns:
    (str) Text with first letter capitalized.
    '''
    if text:
        return text[0].upper() + text[1:]
    else:
        return text


def get_word_index(text, char_index):
    '''
    Args:
    (str) text -- Text.
    (int) char_index -- Character index.

    Returns:
    (int) Word index corresponding to character index.
    '''
    if char_index == -1:
        return -1

    space_count = 0
    for i, c in enumerate(text):
        if i == char_index:
            break
        if c == ' ':
            space_count += 1
    return space_count


def normalize(text):
    '''
    Args:
    (str) text -- Process this text.

    Returns:
    (str) Text in lowercase without punctuations.
    '''
    return re.sub(r'[^\w\s]', '', text).lower()


def equals(text0, text1):
    '''
    Args:
    (str) text0 -- Text to compare.
    (str) text1 -- Text to compare.

    Returns:
    (bool) True if normalized texts are equal, else false.
    '''
    return normalize(text0) == normalize(text1)


def remove_punctuations(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text without punctuations.
    '''
    for punctuation in string.punctuation:
        text = text.replace(punctuation, '')
    return text


def replace_digit_text(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text after digit processing.
    '''
    text = _T2D.convert(text)
    text = _replace_digit_decimal(text)
    text = _replace_plus_digit(text)
    text = _replace_digit_plus(text)
    text = _replace_minus_digit(text)
    text = _replace_digit_minus(text)
    text = _replace_digit_slash(text)
    text = _replace_percentage(text)
    return text


def _replace_digit_plus(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text where, for example, "2 plus" is replaced with "2+".
    '''
    matches = re.findall(r'(?:for|to|[0-9]*) positive', text, flags=re.IGNORECASE)
    matches += re.findall(r'(?:for|to|[0-9]*) plus', text, flags=re.IGNORECASE)
    matches += re.findall(r'(?:for|to|[0-9]*) \+', text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if len(elems) == 2:
            digit = _get_numeric_homophone(elems[0])
            text = text.replace(match, f'{digit}+')
    return text


def _replace_plus_digit(text):
    '''
    Args:
    (str) text --  Text

    Returns:
    (str) Text where, for example "plus 2" is replaced with "+2".
    '''
    matches = re.findall(r'positive (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    matches += re.findall(r'plus (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    matches += re.findall(r'\+ (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if len(elems) == 2:
            digit = _get_numeric_homophone(elems[1])
            text = text.replace(match, f'+{digit}')
    return text


def _replace_digit_minus(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text where, for example, "2 minus" is replaced with "2-".
    '''
    matches = re.findall(r'(?:for|to|[0-9]*) negative', text, flags=re.IGNORECASE)
    matches += re.findall(r'(?:for|to|[0-9]*) minus', text, flags=re.IGNORECASE)
    matches += re.findall(r'(?:for|to|[0-9]*) \-', text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if len(elems) == 2:
            digit = _get_numeric_homophone(elems[0])
            text = text.replace(match, f'{digit}-')
    return text


def _replace_minus_digit(text):
    '''
    Args:
    (str) text -- Text with spelled out numbers already converted to
    numeric form.

    Returns:
    (str) Text with "negative [0-9]" converted to "-[0-9]".
    '''
    matches = re.findall(r'negative (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    matches += re.findall(r'minus (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    matches += re.findall(r'\- (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if len(elems) == 2:
            digit = _get_numeric_homophone(elems[1])
            text = text.replace(match, f'-{digit}')
    return text


def _replace_digit_decimal(text):
    '''
    Args:
    (str) text -- Text with spelled out numbers already converted to
    numeric form.

    Returns:
    (str) Text with "[0-9]* point [0-9]*" converted to "[0-9]*.[0-9]*"
    '''
    matches = re.findall(r'(?:for|to|[0-9]*) point (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    matches += re.findall(r'(?:for|to|[0-9]*) dot (?:for|to|[0-9]*)', text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if len(elems) == 3:
            num0 = _get_numeric_homophone(elems[0])
            num1 = _get_numeric_homophone(elems[2])
            text = text.replace(match, f'{num0}.{num1}')
    return text


def _get_numeric_homophone(text):
    '''
    Args:
    (str) text -- Some number.

    Returns:
    (str) The numeric homophone of the text.
    '''
    mod = text.lower()
    if mod == 'to':
        return '2'
    elif mod == 'for':
        return '4'
    else:
        return text


def _replace_digit_slash(text):
    '''
    Args:
    (str) text -- Text with expressions like "4 out of 4" and "120 over 80".

    Returns:
    (str) Text with, for example, "4/4" and "120/80".
    '''
    ex = r'(?:for|to|[0-9]*)[-+]? (?:over|out of|out|of|outta) (?:for|to|[0-9]*)[-+]?'
    matches = re.findall(ex, text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if ('out of' in match.lower() and len(elems) == 4) or len(elems) == 3:
            num0 = _get_numeric_homophone(elems[0])
            num1 = _get_numeric_homophone(elems[-1])
            text = text.replace(match, f'{num0}/{num1}')
    return text


def _replace_percentage(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text with "5 percent" replaced with "5%".
    '''
    matches = re.findall(r'(?:for|to|[0-9]*) percent', text, flags=re.IGNORECASE)
    for match in matches:
        elems = match.split()
        if len(elems) == 2:
            digit = _get_numeric_homophone(elems[0])
            text = text.replace(match, f'{digit}%')
    return text


def remove_tokens(doc):
    '''
    Args:
    (obj) doc -- SpaCy doc object.

    Returns:
    (obj) Doc without tokens whose 'delete' attributes are set to true.
    '''
    spaces = []
    keep = [token for token in doc if not token._.delete]
    for token in keep:
        if token.whitespace_:
            spaces.append(True)
        elif token.i + 1 < len(doc) and token.nbor(1)._.delete:
            spaces.append(True)
        else:
            spaces.append(False)
    return Doc(doc.vocab, words=[token.text for token in keep], spaces=spaces)


def first_letter_is_cap(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (bool) True if first letter in text is capitalized.
    '''
    for c in text:
        if c.isalpha():
            if c.isupper():
                return True
            else:
                return False
    return False
