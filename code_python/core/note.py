'''
Create encounter note.
'''

import json

from titlecase import titlecase

from core.exam import get_exam, EXAM_CUES
from core.summary import has_summary, get_summary

from core.section import (
    get_history_timelog,
    get_exam_timelog,
    CC_CUE,
    HPI_CUE,
    PMH_CUE,
    PSH_CUE,
    FHX_CUE,
    SHX_CUE,
    MED_CUE,
    ALG_CUE,
    ROS_CUE,
    ANP_CUE,
    MID_CUE,
    EXAM_CUE)

# Summary cues.
SUMMARY_INIT_CUES = [
    'summarized',
    'summarize',
    'summary',
    'summaries',
    'summarizes',
    'recap',
    'recaps',
    'recapped',
    'recapping'
]

# Cues to split stream by.
_STREAM_SPLIT_CUES = SUMMARY_INIT_CUES + EXAM_CUES


class _Note:
    '''
    Encounter note.
    '''
    def __init__(self):
        '''
        Attrs:
        (list: chunk) chunks -- Encounter note portions.
        '''
        self._data = []

    def to_json(self):
        '''
        Returns:
        (str) JSON representation of the note.
        '''
        return json.dumps(self._data)

    def add(self, head, body):
        '''
        Add head and body.

        Args:
        (str) head -- Section head.
        (str or list) body -- Section body.
        '''
        self._data.append({
            'head': titlecase(head),
            'body': body
        })
    

def get_note(streams, doctag, gender, template={}):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Physician speaker tag.
    (bool) gender -- True if male, false if female.
    (json) template -- Note template.

    Returns:
    (Note) Note.
    '''
    note = _Note()
    exam = get_exam(streams, doctag, gender, template)
    exam_timelog = get_exam_timelog(streams, doctag)
    history = _get_history(streams, doctag, gender, exam_timelog)
    anp = _get_anp(streams, doctag, gender, exam_timelog)

    _add_history_section(note, CC_CUE, history, template)
    _add_history_section(note, HPI_CUE, history, template)
    _add_history_section(note, PMH_CUE, history, template)
    _add_history_section(note, PSH_CUE, history, template)
    _add_history_section(note, FHX_CUE, history, template)
    _add_history_section(note, SHX_CUE, history, template)
    _add_history_section(note, MID_CUE, history, template)
    _add_history_section(note, MED_CUE, history, template)
    _add_history_section(note, ALG_CUE, history, template)
    _add_history_section(note, ROS_CUE, history, template)

    note.add(EXAM_CUE, exam)
    note.add(ANP_CUE, anp)

    return note


def get_blank_note(template={}):
    '''
    Args:
    (json) template -- Note template.

    Returns:
    (Note) Blank note.
    '''
    note = _Note()
    note.add(CC_CUE, template.get(CC_CUE, ''))
    note.add(HPI_CUE, template.get(HPI_CUE, ''))
    note.add(PMH_CUE, template.get(PMH_CUE, ''))
    note.add(PSH_CUE, template.get(PSH_CUE, ''))
    note.add(FHX_CUE, template.get(FHX_CUE, ''))
    note.add(SHX_CUE, template.get(SHX_CUE, ''))
    note.add(MED_CUE, template.get(MED_CUE, ''))
    note.add(ALG_CUE, template.get(ALG_CUE, ''))
    note.add(ROS_CUE, template.get(ROS_CUE, ''))
    note.add(EXAM_CUE, '')
    note.add(ANP_CUE, '')
    return note


def _add_history_section(note, cue, history, template):
    '''
    Add section to note if in history obj or template obj, prioritizing
    info from history. If section is not in either object, do not add
    the section name.

    Args:
    (Note) note -- Note.
    (str) cue -- Section cue.
    (dict: str, str) history -- History summaries.
    (json) template -- Note template.
    '''
    aka = cue
    if cue == PMH_CUE:
        aka = 'Past Medical History'
    elif cue == PSH_CUE:
        aka = 'Past Surgical History'
    if cue in history:
        note.add(aka, history[cue])
    elif cue in template:
        note.add(aka, template[cue])
    elif cue != MID_CUE:
        note.add(aka, '')


def _get_history(streams, doctag, gender, exam_timelog):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Speaker tag for physician.
    (bool) gender -- True if patient is male, else false.
    (ExamTimelog) exam_timelog -- Object for checking when exam is
    started, paused, or stopped.

    Returns:
    (dict: str, str) History summaries. Key is the section title. Value
    is the section body.
    '''
    sections = {}
    history_timelog = get_history_timelog(streams, doctag)
    for stream in streams:
        if stream.speaker_tag == doctag:
            for substream in stream.split(_STREAM_SPLIT_CUES):
                if (has_summary(substream.text, SUMMARY_INIT_CUES)
                        and not exam_timelog.stopped(substream.start_time)):
                    summary = get_summary(substream.text, gender, SUMMARY_INIT_CUES)
                    section = _get_history_section(
                        substream.start_time, history_timelog, exam_timelog)
                    sections.setdefault(section, '')
                    sections[section] += summary + ' '
    return sections


def _get_history_section(time, history_timelog, exam_timelog):
    '''
    Args:
    (float) time -- Get history section for this time.

    (Timelog) history_timelog -- Object for getting history sections
    by time.

    (ExamTimelog) exam_timelog -- Object for checking when exam is
    started, paused, or stopped.

    Returns:
    (str) History section for specified time.
    '''
    if exam_timelog.paused(time):
        return MID_CUE
    else:
        return history_timelog.get_section_name(time)


def _get_anp(streams, doctag, gender, exam_timelog):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Speaker tag for physician.
    (bool) gender -- True if patient is male, else false.
    (ExamTimelog) exam_timelog -- Object for checking when exam is
    started, paused, or stopped.

    Returns:
    (str) Summaries for assessment and plan.
    '''
    anp = ''
    for stream in streams:
        if stream.speaker_tag == doctag:
            for substream in stream.split(_STREAM_SPLIT_CUES):
                if (has_summary(substream.text, SUMMARY_INIT_CUES)
                        and exam_timelog.stopped(substream.start_time)):
                    summary = get_summary(substream.text, gender, SUMMARY_INIT_CUES)
                    anp += summary + ' '
    return anp


def _get_summary_times(streams, doctag):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Speaker tag for physician.

    Returns:
    (list: tuple: float, float) Start and end times for each summary
    statement.
    '''
    times = []
    for stream in streams:
        if stream.speaker_tag == doctag:
            for substream in stream.split(_STREAM_SPLIT_CUES):
                if has_summary(substream.text, SUMMARY_INIT_CUES):
                    times.append((substream.start_time, substream.end_time))
    return times
