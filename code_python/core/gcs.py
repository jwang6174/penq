'''
Use Google Cloud speech-to-text API.
'''

import os
from time import time
from uuid import uuid4

from google.cloud.speech_v1p1beta1 import SpeechClient
from google.cloud.speech_v1p1beta1.types import RecognitionAudio, RecognitionConfig
from google.cloud import storage

from core.stream import StreamWord, Stream
from util.pathutil import KEYS_DIR

# Set up Google credentials.
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = \
os.path.join(KEYS_DIR, 'digital-scribe-00-7572213495e0.json')

# Bucket name.
_BUCKET_NAME = 'digital-scribe-bucket'

# Determine which function to use to create streams.
STREAM_MODE = 1


def transcribe(content, people, encounter_type, output):
    '''
    Create conversation transcript using Google speech-to-text.

    Args:
    (obj) content -- Audio content.
    (int) people -- Number of speakers in recording.
    (str) encounter_type -- 'in-person'  or 'telemedicine'.
    (bool) output -- True if output should be printed, else false.

    Returns:
    (obj) Google speech-to-text response object.
    '''
    client = SpeechClient()
    blob, uri = _upload(content, _BUCKET_NAME, uuid4().hex)
    audio = RecognitionAudio(uri=uri)
    if encounter_type == 'in-person':
        audio_channel_count = 1
        enable_speaker_diarization = True
        enable_separate_recognition_per_channel = False
    elif encounter_type == 'telemedicine':
        audio_channel_count = 2
        enable_speaker_diarization = False
        enable_separate_recognition_per_channel = True
    else:
        raise ValueError('Invalid encounter type.')
    config = RecognitionConfig(
        model='video',
        use_enhanced=True,
        enable_automatic_punctuation=True,
        enable_speaker_diarization=enable_speaker_diarization,
        diarization_speaker_count=people,
        audio_channel_count=audio_channel_count,
        enable_separate_recognition_per_channel=enable_separate_recognition_per_channel,
        enable_word_time_offsets=True,
        language_code='en-US')
    start = time()
    operation = client.long_running_recognize(config, audio)
    response = operation.result(timeout=3600)
    end = time()
    print(f'Transcribe time: {end - start}')
    if output:
        _display(response)
    blob.delete()
    return response


def _upload(content, bucket_name, blob_name):
    '''
    Upload audio to Google Cloud.

    Args:
    (obj) content -- Audio content.
    (str) bucket_name -- Bucket name.
    (str) blob_name -- Blob name.

    Returns:
    (obj) Blob.
    '''
    for i in range(5):
        try:
            start = time()
            client = storage.Client()
            bucket = client.bucket(bucket_name)
            blob = bucket.blob(blob_name)
            blob.upload_from_string(content)
            end = time()
            print(f'Upload time: {end - start}')
            return blob, f'gs://{bucket_name}/{blob_name}'
        except:
            print('Error uploading audio. Retrying...')
            pass


def _display(response):
    '''
    Print response attributes if output is true.

    Args:
    (obj) response -- Google speech-to-text response object.
    '''
    for i, result in enumerate(response.results):
        alternative = result.alternatives[0]
        print('-' * 50)
        print(f'1st alternative of result {i}')
        print(f'transcript: {alternative.transcript}')
        for word in alternative.words:
            print(
                f'word: {word.word}, '
                f'speaker_tag: {word.speaker_tag}, '
                f'start_time: {word.start_time.seconds}')


def get_streams(response, encounter_type):
    '''
    There are two modes for getting streams. This is because the Google
    speech-to-text services has inconsistent behavior with whether all
    words are in the last result.

    Args:
    (obj) response -- Google speech-to-text response object.
    (str) encounter_type -- 'in-person'  or 'telemedicine'.

    Returns:
    (list: Stream) Segments of uninterrupted text from each speaker.
    '''
    if STREAM_MODE == 1:
        return _get_streams_1(response, encounter_type)
    elif STREAM_MODE == 2:
        return _get_streams_2(response, encounter_type)
    else:
        raise Exception('Invalid stream mode.')


def _get_streams_1(response, encounter_type):
    '''
    Mode 1. Assumes that each result contains only its own words.

    Args:
    (obj) response -- Google speech-to-text response object.
    (str) encounter_type -- 'in-person'  or 'telemedicine'.

    Returns:
    (list: Stream) Segments of uninterrupted text from each speaker.

    '''
    words = []
    streams = []
    for result in response.results[:-1]:
        for word in result.alternatives[0].words:
            if encounter_type == 'telemedicine':
                if not words or result.channel_tag == words[0].speaker_tag:
                    words.append(StreamWord(word, result.channel_tag))
                elif result.channel_tag != words[0].speaker_tag:
                    stream = Stream(words)
                    streams.append(stream)
                    words = [StreamWord(word, result.channel_tag)]
            elif encounter_type == 'in-person':
                if not words or word.speaker_tag == words[0].speaker_tag:
                    words.append(StreamWord(word, word.speaker_tag))
                elif word.speaker_tag != words[0].speaker_tag:
                    stream = Stream(words)
                    streams.append(stream)
                    words = [StreamWord(word, word.speaker_tag)]
            else:
                raise ValueError('Invalid encounter type.')
    if words:
        stream = Stream(words)
        streams.append(stream)
    return streams


def _get_streams_2(response, encounter_type):
    '''
    Mode 2. Assumes that the last result contains its own words and
    words of previous results.

    Args:
    (obj) response -- Google speech-to-text response object.
    (str) encounter_type -- Type of encounter.

    Returns:
    (list: Stream) Segments of uninterrupted text after physician
    identification sequence.
    '''
    words = []
    streams = []
    if response.results and encounter_type == 'in-person':
        for word in response.results[-1].alternatives[0].words:
            if not words or word.speaker_tag == words[0].speaker_tag:
                words.append(StreamWord(word, word.speaker_tag))
            elif word.speaker_tag != words[0].speaker_tag:
                stream = Stream(words)
                streams.append(stream)
                words = [StreamWord(word, word.speaker_tag)]
        if words:
            stream = Stream(words)
            streams.append(stream)
    return streams
