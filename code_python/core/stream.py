from re import finditer

from core.textutil import get_word_index, get_medical_soundalike


class StreamWord:
    '''
    Word and timestamp.
    '''
    def __init__(self, word, speaker_tag):
        '''
        Args:
        (obj) word -- Google speech-to-text word object.

        Attrs:
        (str) text -- Word.
        (float) time -- Word timestamp.
        (int) speaker_tag -- Identifies the person who said this word.
        '''
        self._text = get_medical_soundalike(word.word)
        self._time = word.start_time.seconds + word.start_time.nanos / 1_000_000_000
        self._speaker_tag = speaker_tag

    @property
    def text(self):
        '''
        Returns:
        (str) Word.
        '''
        return self._text

    @text.setter
    def text(self, s):
        '''
        Args:
        (str) s -- Some string.
        '''
        self._text = s

    @property
    def time(self):
        '''
        Returns:
        (float) Word timestamp.
        '''
        return self._time

    @property
    def speaker_tag(self):
        '''
        Returns:
        (int) Identifier for the person who said this word.
        '''
        return self._speaker_tag


class Stream:
    '''
    Segment of uninterrupted text from a speaker.
    '''
    def __init__(self, words):
        '''
        Args:
        (list: StreamWord) words -- List of StreamWord ojects.
        '''
        self._words = words
        self._speaker_tag = words[0].speaker_tag

    @property
    def speaker_tag(self):
        return self._speaker_tag
    
    @property
    def words(self):
        return self._words

    @property
    def start_time(self):
        return self.words[0].time

    @property
    def end_time(self):
        return self.words[-1].time

    @property
    def text(self):
        return ' '.join([word.text for word in self.words])

    def split(self, cues):
        '''
        Args:
        (list: str) cues -- Phrases for segmenting text.

        Returns:
        (list: Stream) Sub-streams.
        '''
        substreams = []
        matches = self._get_matches(cues)
        bounds = self._get_bounds(matches)
        for bound0, bound1 in zip(bounds, bounds[1:]):
            init_index = get_word_index(self.text, bound0)
            stop_index = get_word_index(self.text, bound1)
            if stop_index < len(self.words) - 1:
                words = self.words[init_index:stop_index]
            else:
                words = self.words[init_index:]
            substreams.append(Stream(words))
        return substreams

    def _get_matches(self, cues):
        '''
        Args:
        (list: str) cues -- Phrases for segmenting text.

        Returns:
        (list: obj) Cue matches.
        '''
        matches = []
        for cue in cues:
            for match in finditer(cue, self.text.lower()):
                matches.append(match)
        return matches

    def _get_bounds(self, matches):
        '''
        Args:
        (list: obj) matches -- Cue matches.

        Returns:
        (list: int) Match bounds.
        '''
        bounds = set([match.start() for match in matches])
        bounds.add(0)
        bounds.add(len(self.text))
        bounds = list(bounds)
        bounds.sort()
        return bounds
