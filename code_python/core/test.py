'''
Execute test cases.
'''

import os
import json

from core.contraction import expand
from core.inflection import inflect
from core.disfluency import del_disfluencies
from core.textutil import replace_digit_text
from util.pathutil import TEST_DIR


def test_inflections():
    '''
    Test inflection cases.
    '''
    gender = True
    path = os.path.join(TEST_DIR, 'inflection.json')
    with open(path, 'r') as f:
        for case in json.load(f):
            input_ = case['in']
            actual = inflect(input_, gender)
            expected = case['out']
            if actual != expected:
                print('-' * 50)
                print(f'input: {input_}')
                print(f'actual: {actual}')
                print(f'expected: {expected}')


def test_contractions():
    '''
    Test contraction cases.
    '''
    path = os.path.join(TEST_DIR, 'contraction.json')
    with open(path, 'r') as f:
        for case in json.load(f):
            input_ = case['in']
            actual = expand(input_)
            expected = case['out']
            if actual != expected:
                print('-' * 50)
                print(f'input: {input_}')
                print(f'actual: {actual}')
                print(f'expected: {expected}')


def test_disfluencies():
    '''
    Test disfluency cases.
    '''
    path = os.path.join(TEST_DIR, 'disfluency.json')
    with open(path, 'r') as f:
        for case in json.load(f):
            input_ = case['in']
            actual = del_disfluencies(input_)
            expected = case['out']
            if actual != expected:
                print('-' * 50)
                print(f'input: {input_}')
                print(f'actual: {actual}')
                print(f'expected: {expected}')


def test_numbers():
    '''
    Test number cases.
    '''
    path = os.path.join(TEST_DIR, 'numbers.json')
    with open(path, 'r') as f:
        for case in json.load(f):
            input_ = case['in']
            actual = replace_digit_text(input_)
            expected = case['out']
            if actual != expected:
                print('-' * 50)
                print(f'input: {input_}')
                print(f'actual: {actual}')
                print(f'expected: {expected}')
