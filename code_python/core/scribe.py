'''
Analyze audio to create conversation transcript, then generate an
encounter note.
'''

import os
import traceback
from time import time

import core.gcs as gcs

from core.gcs import transcribe, get_streams
from core.note import get_note, get_blank_note
from core.convo import get_doctag


def scribe(audio, people, gender, encounter_type, gcs_log=True, template={}):
    '''
    Create encounter note.

    Args:
    (obj) audio -- Audio.
    (int) people -- Number of people speaking.
    (int) gender -- Patient gender; 1 = male, 2 = female.
    (str) encounter_type -- 'in-person'  or 'telemedicine'.
    (json) template -- Note template.

    Returns:
    (Note) Encounter note.
    (str) Conversation transcript.
    '''
    attempts = []
    response = gcs.transcribe(audio, people, encounter_type, gcs_log)
    for mode in [1, 2]:
        start = time()
        gcs.STREAM_MODE = mode
        streams = get_streams(response, encounter_type)
        transcript = _format_transcript(streams)
        try:
            doctag = get_doctag(streams)
            note = get_note(streams, doctag, gender, template)
            attempts.append((note, transcript))
        except:
            traceback.print_exc()
            note = get_blank_note(template)
            attempts.append((note, transcript))
            pass
        end = time()
        print(f'Note time {mode}: {end - start}')
    return _get_best_attempt(attempts)


def _get_best_attempt(attempts):
    '''
    This is necessary because Google speech-to-text produces inconsistent
    behavior with the alternative words. Sometimes the list includes words
    for the entire audio file, other times it only includes words for that
    particular alternative.

    Returns:
    (Note) Best attempt to create a note.
    (str) Conversation transcript of best note.
    '''
    best_note = None
    best_transcript = None
    for note, transcript in attempts:
        if best_note is None or best_note.to_json() <= note.to_json():
            best_note = note
            best_transcript = transcript
    return best_note, best_transcript


def _format_transcript(streams):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.

    Returns:
    (str) Formatted transcript.
    '''
    transcript = []
    for stream in streams:
        transcript.append(f'Person {stream.speaker_tag}: {stream.text}')
    return '\n'.join(transcript)
