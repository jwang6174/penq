'''
Handle disfluencies when converting spoken sentence to written sentence.
'''

import re
import pycurl
from io import BytesIO
from string import punctuation
from urllib.parse import urlencode

from nltk.tree import Tree

from core.textutil import (
    NLP,
    cap_first,
    add_period,
    remove_tokens,
    normalize
)


def del_disfluencies(sentence):
    '''
    Args:
    (str) sentence -- Sentence

    Returns:
    (str) Sentence without disfluencies.
    '''
    mod = []
    for subsentence in NLP(sentence).sents:
        text = subsentence.text
        text = _del_consecutive_repeats(text)
        text = _del_filler_independents(text)
        text = _del_filler_so(text)
        text = _del_filler_like(text)
        text = _del_filler_well(text)
        text = _del_filler_and(subsentence)
        if text:
            mod.append(text)
    # mod = _del_jamshid19_disfluencies(mod)
    return ' '.join(mod)


def _del_jamshid19_disfluencies(raw_sentences):
    '''
    Args:
    (list: str) raw_sentences -- Sentences that need to be processed by
    Jamshid2019 dislfuency parser.

    Returns:
    (list: str) Processed sentences.
    '''
    try:
        new_sentences = []
        in_ = urlencode({'q': '|'.join(raw_sentences)})
        out = _ask_jamshid2019_server(in_)
        if out:
            trees = [Tree.fromstring(s) for s in out.split('|')]
            for tree in trees:
                keep = []
                _traverse_jamshid2019(tree, keep)
                new_sentence = _get_sentence(keep)
                if new_sentence:
                    new_sentences.append(new_sentence)
            return new_sentences
        else:
            return raw_sentences
    except:
        return raw_sentences


def _ask_jamshid2019_server(params):
    '''
    Args:
    (str) params -- Query parameter containing sentences to be analyzed
    by the Jamshid2019 parser.

    Returns:
    (str) Jamshid2019 response.
    '''
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, f'localhost:9001?{params}')
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    return body.decode('iso-8859-1')


def _traverse_jamshid2019(tree, keep):
    '''
    Identify words from the original sentence to keep, based on the
    tree labels.

    Args:
    (obj) tree -- NTLK tree object.

    (list: str) keep -- Words in the tree to keep i.e. words that are
    not labelled EDITED or INTJ.
    '''
    for subtree in tree:
        if _is_terminal_node(subtree):
            keep.append(subtree.leaves()[0])
        elif subtree.label() not in ["EDITED", "INTJ", "PRN"]:
            _traverse_jamshid2019(subtree, keep)


def _del_consecutive_repeats(sentence):
    '''
    Remove word and phrase repeats, with the exception of double
    negatives (i.e. "no no" and "not not").

    Example:

    this is a sentence sentence sentence this is a sentence where 
    phrases phrases duplicate where phrases duplicate

    -> this is a sentence where phrases duplicate

    Args:
    (str) sentence -- Sentence.

    Returns:
    (str) Sentence without consecutive repeats.
    '''
    ex = r'(?!no no|not not)\b(.+)(\s+\1\b)+'
    while re.search(ex, sentence, flags=re.IGNORECASE):
        sentence = re.sub(ex, r'\1', sentence, flags=re.IGNORECASE)
    sentence = add_period(sentence)
    sentence = cap_first(sentence)
    return sentence


def _del_filler_independents(sentence):
    '''
    Args:
    (str) sentence -- Sentence.

    Returns:
    (str) Sentence without consentence-independent filler words.
    '''
    independent_filler_words = [
        'sort of',
        'sorta',
        'kind of',
        'kinda',
        'very',
        'really',
        'basically',
        'literally',
        'honestly',
        'absolutely',
        'totally',
        'super'
    ]
    sentence = ' ' + sentence
    for word in independent_filler_words:
        sentence = re.sub(' ' + word, '', sentence, flags=re.IGNORECASE)
    sentence = add_period(sentence)
    sentence = cap_first(sentence)
    return sentence.lstrip()


def _del_filler_so(sentence):
    '''
    Args:
    (str) sentence -- Sentence.

    Returns:
    (str) Sentence without "so" at the beginning.
    '''
    words = sentence.split()
    if words and words[0].lower() == 'so':
        sentence = ' '.join(words[1:])
        sentence = add_period(sentence)
        sentence = cap_first(sentence)
        return sentence
    else:
        return sentence


def _del_filler_like(sentence):
    '''
    Args:
    (str) sentence -- Sentence.

    Returns:
    (str) Sentence without filler "like".
    '''
    doc = NLP(sentence)
    for token in doc:
        if token.text.lower() == 'like' and token.dep_ != 'prep' and token.dep_ != 'ROOT':
            token._.delete = True
    sentence = remove_tokens(doc).text
    sentence = add_period(sentence)
    sentence = cap_first(sentence)
    return sentence


def _del_filler_well(sentence):
    '''
    Args:
    (str) sentence -- Sentence.

    Returns:
    (str) Sentence without filler "well".
    '''
    doc = NLP(sentence)
    for token in doc:
        if token.text.lower() == 'well' and token.dep_ != 'advmod' and token.dep_ != 'ROOT' and token.dep_ != 'acomp':
            token._.delete = True
    sentence = remove_tokens(doc).text
    sentence = add_period(sentence)
    sentence = cap_first(sentence)
    return sentence


def _del_filler_and(sentence):
    '''
    Args:
    (obj) sentence -- SpaCy sentence object.

    Returns:
    (str) Sentence where filler "and" is substituted with a period and
    capitalized next word.
    '''
    if sentence.text.lower().count('and') >= 1:
        tree = Tree.fromstring(sentence._.parse_string)
        splits = _get_and_splits(tree)
        split_count = 0
        sentences = []
        temporary = []
        for i, word in enumerate(sentence.text.split()):
            if normalize(word) == 'and':
                if splits[split_count]:
                    if temporary and temporary[0].lower() == 'and':
                        temporary.pop(0)
                    sentence = _get_sentence(temporary)
                    sentences.append(sentence)
                    del temporary[:]
                elif i > 0:
                    temporary.append(word)
                split_count += 1
            else:
                temporary.append(word)
        if temporary:
            sentence = _get_sentence(temporary)
            sentences.append(sentence)
        out = ' '.join(sentences)
        return out
    else:
        return sentence.text


def _get_sentence(temporary):
    '''
    Args:
    (list: str) temporary -- Words to be in a sentence.

    Returns:
    (str) Sentence.
    '''
    sentence = ''
    for word in temporary:
        if word not in punctuation:
            sentence += ' '
        sentence += word
    sentence = sentence.strip()
    sentence = cap_first(sentence)
    sentence = add_period(sentence)
    return sentence


def _get_and_splits(tree):
    '''
    Args:
    (obj) tree -- NLTK tree object.

    Returns:
    (list: bool) If the first element is True, then the original
    sentence should be split at the first "and".
    '''
    splits = []
    _traverse_and(tree, splits)
    return splits


def _traverse_and(tree, splits):
    '''
    Populate splits list for filler "and".

    Args:
    (obj) tree -- NLTK tree object.

    (list: bool) splits -- If the first element is True, then the
    original setnence should be split at the first "and".    
    '''
    for i in range(0, len(tree)):
        this_subtree = tree[i]
        if not isinstance(this_subtree, str):
            if _is_and_node(this_subtree):
                if 0 < i < len(tree) - 1:
                    prev_subtree = tree[i - 1]
                    next_subtree = tree[i + 1]
                    if _is_sentence_node(prev_subtree) and _is_sentence_node(next_subtree):
                        splits.append(True)
                    else:
                        splits.append(False)
                else:
                    splits.append(False)
            _traverse_and(this_subtree, splits)


def _is_and_node(tree):
    '''
    Args:
    (obj) tree -- NLTK tree object.

    Returns:
    (bool) True if tree is a terminal node and has the word "and".
    '''
    return _is_terminal_node(tree) and tree.leaves()[0].lower() == 'and'


def _is_sentence_node(tree):
    '''
    Args:
    (obj) tree -- NLTK tree object.

    Returns:
    (bool) True if tree has the label "S", which represents an
    independent clause.
    '''
    return tree.label() == 'S'


def _is_terminal_node(tree):
    '''
    Args:
    (obj) tree -- NLTK tree object.

    Returns:
    (bool) True if tree is a terminal node, else false.
    '''
    if len(tree) != 1:
        return False
    else:
        if not isinstance(tree[0], str):
            return False
        else:
            return True
