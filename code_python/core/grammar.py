'''
Module for grammar utility functions.
'''

from core.disfluency import del_disfluencies
from core.textutil import NLP, normalize

# Modal verbs do not change from second person singular to third person
# singular form.
# 
# Ex: You can go to the store. -> He can go to the store.
MODAL_VERBS = [
    'can',
    'must',
    'should',
    'may',
    'might',
    'will',
    'shall',
    'would',
    'could'
]

# Auxiliary verbs do change from second person singular form to third
# person singular form.
# 
# Ex: You are ready to quit smoking. -> He is ready to quit smoking.
SECOND_PERSON_AUX_VERBS = {
    'are': 'is',
    'have': 'has',
    'do': 'does',
    'were': 'was'
}

# Words that may start a question.
QUESTION_WORDS = [
    'who',
    'where',
    'when',
    'why',
    'what',
    'which',
    'how'
]


class GenderRules:
    '''
    Grammar rules for a person's gender.
    '''
    def __init__(self, gender):
        '''
        Args:
        (int) gender -- 1 = male, 2 = female.

        Attrs:
        (int) gender -- 1 = male, 2 = female.
        (str) pronoun_subject -- He or she.
        (str) pronoun_object -- Him or her.
        (str) pronoun_reflexive -- Himself or herself.
        (str) possessive_pronoun -- His or hers.
        (str) possessive_adjective -- His or her.
        '''
        self._check(gender)
        self.gender = gender
        self.pronoun_subject = self._get_pronoun_subject()
        self.pronoun_object  = self._get_pronoun_object()
        self.pronoun_reflexive = self._get_pronoun_reflexive()
        self.possessive_pronoun = self._get_possessive_pronoun()
        self.possessive_adjective = self._get_possessive_adjective()

    def _check(self, gender):
        '''
        Raise exception for invalid parameters.

        Args:
        (int) gender -- 1 = male, 2 = female.
        '''
        if gender != 1 and gender != 2:
            raise ValueError('Invalid gender code.')

    def _get_pronoun_subject(self):
        '''
        Returns:
        (str) He or she.
        '''
        if self.gender == 1:
            return 'he'
        elif self.gender == 2:
            return 'she'

    def _get_pronoun_object(self):
        '''
        Returns:
        (str) Him or her.
        '''
        if self.gender == 1:
            return 'him'
        elif self.gender == 2:
            return 'her'

    def _get_pronoun_reflexive(self):
        '''
        Returns:
        (str) Himself or herself.
        '''
        if self.gender == 1:
            return 'himself'
        elif self.gender == 2:
            return 'herself'

    def _get_possessive_pronoun(self):
        '''
        Returns:
        (str) His or hers.
        '''
        if self.gender == 1:
            return 'his'
        elif self.gender == 2:
            return 'hers'

    def _get_possessive_adjective(self):
        '''
        Returns:
        (str) His or her.
        '''
        if self.gender == 1:
            return 'his'
        elif self.gender == 2:
            return 'her'


def is_question(sentence):
    '''
    Args:
    (str) sentence -- A sentence.

    Returns:
    (bool) True if sentence is a question, else false.
    '''
    return sentence.endswith('?') or sentence.split()[0].lower() in QUESTION_WORDS


def is_2nd_person_aux(verb):
    '''
    Args:
    (str) verb -- Check if this verb is an auxiliary verb.

    Returns:
    (bool) True if verb is an auxiliary verb, else false.
    '''
    return verb.lower() in SECOND_PERSON_AUX_VERBS


def is_modal(verb):
    '''
    Args:
    (str) verb -- Check if this is a modal verb.

    Returns:
    (bool) True if verb is modal, else false.
    '''
    return normalize(verb) in MODAL_VERBS


def is_imperative(sentence):
    '''
    Args:
    (str) sentence -- Sentence.

    Returns:
    (bool) True if sentence is imperative; that is, starts with verb in
    infinitive form or 'do not'.
    '''
    sentence = del_disfluencies(sentence)
    doc = NLP(sentence)
    norm = normalize(sentence)
    if len(doc):
        is_verb = doc[0].pos_ == 'VERB'
        is_infinitive = (doc[0].text.lower() == doc[0].lemma_.lower())
        is_negation = norm.startswith('dont') or norm.startswith('do not')
        is_polite = norm.startswith('please') or norm.endswith('please')
        return (is_verb and is_infinitive) or is_negation or is_polite
    else:
        return False