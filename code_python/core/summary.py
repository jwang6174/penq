import re
import string

from core.contraction import expand
from core.disfluency import del_disfluencies
from core.grammar import is_question
from core.inflection import inflect

from core.textutil import (
    NLP,
    cap_first,
    add_period,
    replace_digit_text,
    replace_medical_soundalikes,
    get_word_index)

from core.exam import (
    INIT_CUES as EXAM_INIT_CUES,
    STOP_CUES as EXAM_STOP_CUES)

# Summary stop cues. Order of elements matters. Sort by word count.
_STOP_CUES = [
    'is that correct',
    'is that right',
    'is that okay',
    'is that ok',
    'that correct',
    'that right',
    'that okay',
    'that ok'
]


def has_summary(text, init_cues):
    '''
    Args:
    (str) text -- Speaker text.
    (list: str) init_cues -- Init cues.

    Returns:
    (bool) True if speaker text contains an init cue.
    '''
    return _has_init_cue(text, init_cues)


def get_summary(text, gender, init_cues):
    '''
    Args:
    (str) text -- Speaker text.
    (int) gender -- Gender code.
    (list: str) init_cues -- Init cues.

    Returns:
    (str) Text to be written into the note.
    '''
    text = replace_digit_text(text)
    text = replace_medical_soundalikes(text)
    text = expand(text)
    text = _remove_init_cue(text, init_cues)
    text = _remove_stop_cue(text)
    text = text.replace('?', '.')
    text = del_disfluencies(text)
    text = inflect(text, gender)
    text = _remove_exam_cues(text)
    text = _finalize(text)
    return text


def _has_init_cue(text, init_cues):
    '''
    Args:
    (str) text -- Speaker text.
    (list: str) init_cues -- Init cues.

    Returns:
    (bool) True if text contains a init cue, else false.
    '''
    return any(cue in text.lower() for cue in init_cues)


def _remove_init_cue(text, init_cues):
    '''
    Args:
    (str) text -- Text with init cue.
    (list: str) init_cues -- Init cues.

    Returns:
    (str) Text without init cue.
    '''
    init_cue = _get_init_cue(text, init_cues)
    init_idx = text.lower().find(init_cue) + len(init_cue)
    text = text[init_idx:]
    text = text[0].upper() + text[1:]
    return text


def _has_stop_cue(text):
    '''
    Args:
    (str) text -- Check if this text has a stop cue.

    Returns:
    (bool) True if text has a stop cue, else false.
    '''
    return any(cue in text.lower() for cue in _STOP_CUES)


def _remove_stop_cue(text):
    '''
    Args:
    (str) text -- Text possibly ending with stop cue.

    Returns:
    (str) Text without stop cue at the end, if there was one present.
    Else false. Also remove everything else after the stop cue.
    '''
    stop = -1
    for cue in _STOP_CUES:
        chunk = ' ' + cue
        index = text.lower().find(chunk)
        if stop == -1 or 0 <= index < stop:
            stop = index
    if stop >= 0:
        text = text[:stop]
        text = cap_first(text)
        text = add_period(text)
    return text


def _remove_questions(text):
    '''
    Args:
    (str) text -- Speaker text.

    Returns:
    (str) Text without first question and everything else after. If
    there is no question, then returns the original text.
    '''
    keep_sentences = []
    for sentence in NLP(text).sents:
        keep_phrases = []
        for phrase in sentence.text.split(', '):
            if phrase:
                if phrase[-1] != '?':
                    keep_phrases.append(phrase)
                else:
                    if keep_phrases:
                        keep_sentences.append(add_period(', '.join(keep_phrases)))
                    if keep_sentences:
                        return ' '.join(keep_sentences)
                    else:
                        return text
        if keep_phrases:
            keep_sentences.append(add_period(', '.join(keep_phrases)))
    if keep_sentences:
        return ' '.join(keep_sentences)
    else:
        return text


def _get_init_cue(text, init_cues):
    '''
    Args:
    (str) text -- Text with init cue.
    (list: str) init_cues -- Init cues.

    Returns:
    (str) Contained init cue.
    '''
    for cue in init_cues:
        if cue in text.lower():
            return cue
    raise Exception('No init cue found.')


def _finalize(raw):
    '''
    Args:
    (str) Unfinalized note.

    Returns:
    (str) Finalized note.
    '''
    lines = []
    for line in raw.split('\n'):
        sentences = []
        line = line.strip()
        line = line.replace('?', '.')
        line = line.replace('!', '.')
        line = line.replace('Can', 'can')
        line = line.replace('can not', 'cannot')
        for sentence in line.split('. '):
            if len(sentence.strip()) > 1:
                sentence = sentence[0].upper() + sentence[1:]
                sentences.append(sentence)
        lines.append(add_period('. '.join(sentences)))
    text = '\n'.join(lines)
    return text.strip()


def _remove_exam_cues(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    Text without exam cues.
    '''
    text = _trim_cues(text, EXAM_INIT_CUES)
    text = _trim_cues(text, EXAM_STOP_CUES)
    return text


def _trim_cues(text, cues):
    '''
    Args:
    (str) text -- Text to trim.
    (list: str) cues -- Trim these cues from text.

    Returns:
    (str) Text without cues.
    '''
    for cue in cues:
        text = _trim_cue(text, cue)
    return text


def _trim_cue(text, cue):
    '''
    Args:
    (str) text -- Text to trim.
    (str) cue -- Trim this cue from text.

    Returns:
    (str) Text without cue.
    '''
    char_index = text.lower().find(cue)
    word_index = get_word_index(text, char_index)
    if word_index == 0:
        return ''
    elif word_index > 0:
        return ' '.join(text.split()[:word_index])
    else:
        return text
