'''
Determine if sentence has the intent of ascertaining information related to
a specific section of an encounter note, including past medical history,
family history, and social history.
'''

from collections import namedtuple

from core.textutil import get_word_index

from core.exam import (
    INIT_CUES as EXAM_INIT_CUES,
    STOP_CUES as EXAM_STOP_CUES)

# Cue for chief complaint.
CC_CUE = 'chief complaint'

# Cue for history of present illness.
HPI_CUE = 'history of present illness'

# Cue for past medical history.
PMH_CUE = 'medical history'

# Cue for past surgical history.
PSH_CUE = 'surgical history'

# Cue for family history.
FHX_CUE = 'family history'

# Cue for social history.
SHX_CUE = 'social history'

# Cue for medications.
MED_CUE = 'medications'

# Cue for allergies.
ALG_CUE = 'allergies'

# Cue for review of systems.
ROS_CUE = 'review of systems'

# Cue for physical exam.
EXAM_CUE = 'physical exam'

# Cue for assessment and plan.
ANP_CUE = 'assessment and plan'

# Cue for mid-exam summaries.
MID_CUE = 'mid-exam'


# Section and time.
_Section = namedtuple('section', ['name', 'time'])


class _Timelog:
    '''
    Object for getting sections by time.
    '''
    def __init__(self):
        '''
        Attrs:
        (list: Section) Sections.
        '''
        self._sections = []

    @property
    def sections(self):
        '''
        Returns:
        (list: Section) Sections.
        '''
        return self._sections
    
    def add(self, name, time):
        '''
        Args:
        (str) name -- Section name.
        (float) time -- Start time of section.
        '''
        self._sections.append(_Section(name, time))
        self._sections.sort(key=lambda x: x.time)

    def get_section_name(self, time):
        '''
        Args:
        (float) time -- Get relevant section name for this time.

        Returns:
        (Section) Relevant section.
        '''
        if len(self._sections) > 1:
            for i, section in enumerate(self._sections[1:], start=1):
                if section.time > time:
                    return self._sections[i - 1].name
            return self._sections[-1].name
        return self._sections[0].name


class _HistoryTimelog(_Timelog):
    '''
    Object for getting history sections.
    '''
    def __init__(self):
        '''
        Set HPI as the default history section.
        '''
        super().__init__()
        super().sections.append(_Section(HPI_CUE, 0))


class _ExamTimelog(_Timelog):
    '''
    Object for checking when exam is started, paused, or stopped. 
    '''
    def __init__(self):
        '''
        Attrs:
        (float) init_time -- Exam start time.
        (float) stop_time -- Exam stop time.
        '''
        super().__init__()
        self._init_time = None
        self._stop_time = None

    @property
    def init_time(self):
        '''
        Returns:
        (float) Exam start time.
        '''
        return self._init_time

    @property
    def stop_time(self):
        '''
        Returns:
        (float) Exam stop time.
        '''
        return self._stop_time
    
    def add(self, name, time):
        '''
        Add section name and its start time. Also set the exam start
        time and exam stop time, if necessary.

        Args:
        (str) name -- Section name.
        (float) time -- Start time of section.
        '''
        super().add(name, time)
        self._set_init_time(name, time)
        self._set_stop_time(name, time) 

    def _set_init_time(self, name, time):
        '''
        Set exam start time, if appropriate.

        Args:
        (str) name -- Section name.
        (float) time -- Start time of section.
        '''
        if self._init_time is None and name in EXAM_INIT_CUES:
            self._init_time = time

    def _set_stop_time(self, name, time):
        '''
        Set exam stop time, if appropriate.

        Args:
        (str) name -- Section name.
        (float) time -- Start time of section.
        '''
        if name in EXAM_STOP_CUES:
            self._stop_time = time

    def started(self, time):
        '''
        Args:
        (float) time -- Check if exam is started at this time.

        Returns:
        (bool) True if exam is started at specified time.
        '''
        if self._init_time is not None:
            return self._init_time < time
        return False

    def stopped(self, time):
        '''
        Args:
        (float) time -- Check if exam is stopped at this time.

        Returns:
        (bool) True if exam is stopped at specified time.
        '''
        if self._stop_time is not None:
            return self._stop_time < time
        return False

    def in_progress(self, time):
        '''
        Args:
        (float) time -- Check if exam is in progress at this time.

        Returns:
        (bool) True if exam is in progress at specified time.
        '''
        return self.started(time) and not self.stopped(time)

    def paused(self, time):
        '''
        Args:
        (float) time -- Check if exam is paused as this time.

        Returns:
        (bool) True if exam is paused at specified time.
        '''
        return (
            self.in_progress(time) 
            and super().get_section_name(time) in EXAM_STOP_CUES)


def _get_cue_time(cue, stream):
    '''
    Args:
    (str) cue -- Cue.
    (Stream) stream -- Segment of uninterrupted text.

    Returns:
    (float) Cue time.
    '''
    char_index = stream.text.lower().find(cue)
    word_index = get_word_index(stream.text, char_index)
    return stream.words[word_index].time


def _fill_timelog(streams, doctag, cues, timelog):
    '''
    Fill empty timelog with specified cues.

    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Speaker tag for physician.
    (list: str) cues -- Section cues.
    (TimeLog) timelog -- Object for getting sections by time.
    '''
    for stream in streams:
        if stream.speaker_tag == doctag:
            for substream in stream.split(cues):
                for cue in cues:
                    if cue in substream.text.lower():
                        cue_time = _get_cue_time(cue, substream)
                        timelog.add(cue, cue_time)


def get_history_timelog(streams, doctag):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Speaker tag for physician.

    Returns:
    (HistoryTimelog) Object for getting history sections by time.
    '''
    timelog = _HistoryTimelog()
    cues = [PMH_CUE, PSH_CUE, FHX_CUE, SHX_CUE]
    _fill_timelog(streams, doctag, cues, timelog)
    return timelog


def get_exam_timelog(streams, doctag):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Speaker tag for physician.

    Returns:
    (ExamTimelog) Object for checking when exam is started, paused, or
    stopped.
    '''
    timelog = _ExamTimelog()
    cues = EXAM_INIT_CUES + EXAM_STOP_CUES
    _fill_timelog(streams, doctag, cues, timelog)
    return timelog
