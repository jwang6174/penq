'''
Create physical exam section of a note.
'''

import json
import os
import pickle

from string import punctuation
from collections import OrderedDict
from titlecase import titlecase
from copy import deepcopy
from stop_words import get_stop_words

from core.contraction import expand
from core.grammar import is_imperative
from core.disfluency import del_disfluencies
from core.inflection import inflect
from training.classify.preprocess import preprocess
from training.classify.collect import NLP
from util.pathutil import MODELS_DIR

from core.textutil import (
    normalize,
    add_period,
    cap_first,
    remove_punctuations,
    get_word_index,
    replace_digit_text,
    replace_medical_soundalikes)

# Cues for starting the physical exam.
INIT_CUES = [
    'begin exam',
    'begins exam',
    'began exam',
    'begun exam',
    'beginning exam',
    'start exam',
    'starts exam',
    'started exam',
    'starting exam',
    'cert exam',
    'certs exam',
    'certed exam',
    'certing exam',
    'star exam',
    'stars exam',
    'starred exam',
    'starring exam',
    'resume exam',
    'resumes exam',
    'resumed exam',
    'resuming exam',
    'restart exam',
    'restarts exam',
    'restarted exam',
    'restarting exam',
    'initiate exam',
    'initiates exam',
    'initiated exam',
    'initiating exam'
]

# Cues for stopping the physical exam.
STOP_CUES = [
    'stop exam',
    'stops exam',
    'stopped exam',
    'stopping exam',
    'finish exam',
    'finishes exam',
    'finished exam',
    'finnishing exam',
    'finnish exam',
    'pause exam',
    'pauses exam',
    'paused exam',
    'pausing exam',
    'end exam',
    'ends exam',
    'ended exam',
    'ending exam',
    'and exam',
    'complete exam',
    'completes exam',
    'completed exam',
    'completing exam',
    'conclude exam',
    'concludes exam',
    'concluded exam',
    'concluding exam'
]

# Phrases to ignore in exam.
_IGNORE_PHRASES = [
    'deep breath'
]

# Cue for vital signs.
_VS_CUE = 'vital signs'

# Cue for creating an exam header.
_HEADER_CUE = 'section'

# Possible multi-word headers.
_MULTI_WORD_HEADERS = [
    'lower extremity',
    'upper extremity',
    'lower extremities',
    'upper extremities'
]

# List of all exam cues.
EXAM_CUES = INIT_CUES + STOP_CUES + [_HEADER_CUE]

# Default exam section.
_DEFAULT_HEADER = 'General'

# Path of classifier for medical versus everyday sentences.
_CLASSIFIER_PATH = os.path.join(MODELS_DIR, 'logistic_regression.model')

# The actual classifier.
_CLASSIFIER = pickle.load(open(_CLASSIFIER_PATH, 'rb'))

# Pause duration in seconds indicating a period.
_PAUSE_CUTOFF = 3

# Immutable set of stop words.
_STOPWORDS = frozenset(get_stop_words('en'))


def get_exam(streams, doctag, gender, template):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.
    (int) doctag -- Doctor speaker tag.
    (bool) gender -- True if male, false if female.
    (json) template -- Note template.

    Returns:
    (list) JSON representation of physical exam section.
    '''
    headers = OrderedDict()
    current = _DEFAULT_HEADER
    is_live = False
    _add_vital_signs(headers, template)
    for stream in deepcopy(streams):
        if stream.speaker_tag == doctag:
            for substream in stream.split(EXAM_CUES):
                if _has_init_cue(substream.text):
                    is_live = True
                elif _has_stop_cue(substream.text):
                    is_live = False
                if is_live:
                    _add_pause_periods(substream)
                    if _has_init_cue(substream.text):
                        trimmed = _trim_init_cue(substream.text)
                    elif _has_stop_cue(substream.text):
                        trimmed = _trim_stop_cue(substream.text)
                    elif _has_cue(substream.text, _HEADER_CUE):
                        trimmed = _trim_cue(substream.text, _HEADER_CUE)
                        current = _get_current_header(trimmed)
                        trimmed = _trim_cue(trimmed, current)
                    else:
                        trimmed = substream.text
                    trimmed = replace_digit_text(trimmed)
                    trimmed = replace_medical_soundalikes(trimmed)
                    sentences = _get_medical_sentences(trimmed, gender)
                    if sentences:
                        headers.setdefault(current, []).append(sentences)
    return _get_json(headers)


def _add_vital_signs(headers, template):
    '''
    Add vital signs from template if exist.

    Args:
    (OrderedDict) headers -- Exam headers and body.
    (json) template -- Note template.
    '''
    vitals = template.get(_VS_CUE)
    if vitals:
        headers[_VS_CUE] = [vitals]


def _get_current_header(text):
    '''
    Args:
    (str) text -- Some text with section cue.

    Returns:
    (str) Section.
    '''
    words = remove_punctuations(text).lower().split()
    for length in range(5, 1, -1):
        header = ' '.join(words[:length])
        if header in _MULTI_WORD_HEADERS:
            return header
    return words[0]


def _get_json(headers):
    '''
    Args:
    (OrderedDict) Physical exam sub-sections.

    Returns:
    (list) JSON-style Python list.
    '''
    data = []
    for head, body in headers.items():
        data.append({
            'head': remove_punctuations(titlecase(head)),
            'body': ' '.join(body)
        })
    return data


def _has_cue(text, cue):
    '''
    Args:
    (str) text -- Text.
    (str) cue -- Cue.

    Returns:
    (bool) True if text starts with a cue, else false.
    '''
    return text.lower().find(cue) == 0


def _has_init_cue(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (bool) True if text starts with an init cue, else false.
    '''
    for cue in INIT_CUES:
        if text.lower().find(cue) == 0:
            return True
    return False


def _has_stop_cue(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (bool) True if text starts with a stop cue, else false.
    '''
    for cue in STOP_CUES:
        if text.lower().find(cue) == 0:
            return True
    return False


def _trim_cue(text, cue):
    '''
    Args:
    (str) text -- Text.
    (str) cue -- Cue to trim from beginning of the stream.

    Returns:
    (str) Text without beginning cue. Capitalize first letter of text
    without cue.
    '''
    trimmed = ' '.join(text.split()[len(cue.split()):])
    if len(trimmed) > 2:
        return cap_first(trimmed)
    else:
        return trimmed


def _trim_init_cue(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text without init cue.
    '''
    for cue in INIT_CUES:
        if text.lower().find(cue) == 0:
            return _trim_cue(text, cue)
    raise Exception('No init cue found.')


def _trim_stop_cue(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (str) Text without stop cue.
    '''
    for cue in STOP_CUES:
        if text.lower().find(cue) == 0:
            return _trim_cue(text, cue)
    raise Exception('No stop cue found.')


def _get_medical_sentences(trimmed, gender):
    '''
    Args:
    (str) trimmed -- Text without cue.
    (bool) gender -- True if male, false if female.

    Returns:
    (str) Medical sentences.
    '''
    medical = []
    nlp_doc = NLP(trimmed)
    not_imp = [i for i in nlp_doc.sents if (not is_imperative(i.text) and not i.text.endswith('?'))]
    prepped = [preprocess(del_disfluencies(i.text)) for i in not_imp]
    if prepped:
        predictions = _CLASSIFIER.predict(prepped)
        data = zip(not_imp, predictions, prepped)
        for sentence, prediction, prepped in data:
            if ((_has_positive_or_negative(prepped) or prediction == 'medical') 
                    and not _all_stop_words(prepped)
                    and len(sentence.text.split()) > 1
                    and not normalize(sentence.text) in _IGNORE_PHRASES):
                text = inflect(sentence.text, gender)
                text = add_period(text)
                text = cap_first(text)
                medical.append(text)
    return ' '.join(medical)


def _has_positive_or_negative(prepped):
    '''
    Args:
    (str) prepped -- Pre-processed string.

    Returns:
    (bool) True if the sentence contains 'positive' or 'negative',
    false otherwise.
    '''
    mod = prepped.lower()
    return (
        'positive' in mod or 
        'negative' in mod or 
        'normal' in mod or 
        'abnormal' in mod)


def _add_pause_periods(stream):
    '''
    Add periods for speech pauses.

    Args:
    (Stream) stream -- Uninterrupted segment of speaker text.
    '''
    if len(stream.words) >= 2:
        for w0, w1 in _get_word_pairs(stream):
            if w0.text[-1] not in punctuation and w1.time - w0.time >= _PAUSE_CUTOFF:
                w0.text += '.'
                w1.text = cap_first(w1.text)


def _get_word_pairs(stream):
    '''
    Args:
    (Stream: stream) -- Uninterrupted segment of speaker text.

    Returns:
    (iter: tuple: StreamWord) Pairs of sequential StreamWord objects.
        '''
    size = 2
    for i in range(len(stream.words) - 1):
        yield stream.words[i : (i + size)]


def _all_stop_words(text):
    '''
    Args:
    (str) text -- Text.

    Returns:
    (bool) True if all words in text are stop words.
    '''
    words = normalize(text).split()
    for word in words:
        if word not in _STOPWORDS:
            return False
    return True
