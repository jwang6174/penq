'''
Conversation utility module.
'''

from core.exam import EXAM_CUES
from core.note import SUMMARY_INIT_CUES
from core.section import (
    HPI_CUE,
    PMH_CUE,
    PSH_CUE,
    FHX_CUE,
    SHX_CUE,
    EXAM_CUE)
from core.summary import has_summary

# List of clinician cues, except summary cues.
CUES = EXAM_CUES + [HPI_CUE, PMH_CUE, PSH_CUE, FHX_CUE, SHX_CUE, EXAM_CUE]


def get_doctag(streams):
    '''
    Args:
    (list: Stream) streams -- Uninterrupted segments of speaker text.

    Returns:
    (int) Speaker tag of the physician.
    '''
    roster = {}
    for stream in streams:
        roster.setdefault(stream.speaker_tag, 0)
        if has_summary(stream.text, SUMMARY_INIT_CUES):
            roster[stream.speaker_tag] += 1
        for cue in CUES:
            if cue in stream.text.lower():
                roster[stream.speaker_tag] += 1
    if roster:
        return max(roster, key=roster.get)
    raise Exception('Error: Could not identify the physician speaker.')
