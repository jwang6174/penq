'''
Convert 2nd person tense to 3rd person tense.
'''

import string

from core.grammar import is_modal, is_2nd_person_aux, SECOND_PERSON_AUX_VERBS, GenderRules
from core.textutil import NLP, normalize, equals


def inflect(text, gender):
    '''
    Args:
    (str) text -- Speaker text.
    (int) gender -- Gender code.

    Returns:
    (str) Text inflected to third person with respect to the patient.
    '''
    mods = []
    for sentence in NLP(text).sents:
        mod = []
        subs = _get_subs(sentence, gender)
        for token in sentence:
            if token.pos_ != 'PUNCT' and token.pos_ != 'NUM':
                sub = subs.get(token.i, token.text)
                if token.text_with_ws.endswith(' '):
                    mod.append(sub + ' ')
                else:
                    mod.append(sub)
            else:
                mod.append(token.text_with_ws)
        mods.append(''.join(mod))
    new = ' '.join(mods)
    new = ' '.join(new.split())
    return new


def _get_subs(sentence, gender):
    '''
    Args:
    (obj) sentence -- SpaCy sentence object.
    (int) gender -- Gender code.

    Returns:
    (dict: int, str) Key - token index; Value - replacement text.
    '''
    subs = {}
    _sub_verbs(subs, sentence)
    _sub_subjects(subs, sentence, gender)
    _sub_objects(subs, sentence, gender)
    _sub_possessives(subs, sentence, gender)
    _sub_reflexives(subs, sentence, gender)
    return subs


def _sub_verbs(subs, sentence):
    '''
    Add verb substitutions. Inflect second person verbs and auxiliaries
    to third person forms.

    Args:
    (dict) subs -- Key - token index; Value - replacement text.
    (obj) sentence -- SpaCy sentence object.
    '''
    for token in sentence:
        if token.dep_ == 'nsubj' and equals(token.text, 'you'):
            if token.head.tag_ == 'VBP':
                if is_2nd_person_aux(token.head.text):
                    subs[token.head.i] = _sub_2nd_person_aux(token.head.text)
                else:
                    subs[token.head.i] = _sub_2nd_person_vbp(token.head.text)
            for child in token.head.children:
                if child.dep_ == 'aux' and is_2nd_person_aux(child.text):
                    subs[child.i] = _sub_2nd_person_aux(child.text)
        elif token.tag_ in ['VBP', 'VB'] and token.dep_ == 'conj' and _has_you_nsubj_at_same_level(token):
            if is_2nd_person_aux(token.head.text):
                subs[token.i] = _sub_2nd_person_aux(token.text)
            else:
                subs[token.i] = _sub_2nd_person_vbp(token.text)
            for child in token.children:
                if child.dep_ == 'aux' and is_2nd_person_aux(child.text):
                    subs[child.i] = _sub_2nd_person_aux(child.text)

    for token in sentence:
        if is_2nd_person_aux(token.text) and sentence[token.i - sentence.start - 1].text.lower() == 'you':
            subs[token.i] = _sub_2nd_person_aux(token.text)


def _has_you_nsubj_at_same_level(token):
    '''
    Args:
    (obj) token -- SpaCy token object.

    Returns:
    (bool) True if token has sibling 'you' nsubj, else false.
    '''
    for sibling in token.head.children:
        if sibling.i != token.i and sibling.dep_ == 'nsubj' and equals(sibling.text, 'you'):
            return True
    return False


def _sub_subjects(subs, sentence, gender):
    '''
    Add subject substitutions. Replace 'you' with 'he' if male and
    'she' if female.

    Args:
    (dict) subs -- Key - token index; Value - replacement text.
    (obj) sentence -- SpaCy sentence object.
    (int) gender -- Gender code.
    '''
    for token in sentence:
        if token.dep_ in ['nsubj', 'nsubjpass']:
            subs[token.i] = _sub_subject(token.text, gender)


def _sub_objects(subs, sentence, gender):
    '''
    Add object substitutions. Replace 'you' with 'him' if male and
    'her' if female.

    Args:
    (dict: int, str) subs -- Key - token index; Value - replacement
    text.
    (obj) sentence -- SpaCy sentence object.
    (int) gender -- Gender code.
    '''
    for token in sentence:
        if token.dep_ in ['dobj', 'pobj', 'dative']:
            subs[token.i] = _sub_object(token.text, gender)


def _sub_possessives(subs, sentence, gender):
    '''
    Add possessive substitutions. Replace 'your' with 'his' if male
    and 'her' if female. Replace 'yours' with 'his' if male and 'hers'
    if female.

    Args:
    (dict: int, str) subs -- Key - token index; Value - replacement
    text.
    (obj) sentence -- SpaCy sentence object.
    (int) gender -- Gender code.
    '''
    for token in sentence:
        if token.tag_ == 'PRP$':
            subs[token.i] = _sub_possessive(token.text, gender)


def _sub_reflexives(subs, sentence, gender):
    '''
    Add reflexive substitutions. Replace 'yourself' with 'himself' or
    'herself.'

    Args:
    (dict: int, str) subs -- Key - token index; Value - replacement
    text.
    (obj) sentence -- SpaCy sentence object.
    (int) gender -- Gender code.
    '''
    for token in sentence:
        if token.text.lower() == 'yourself':
            subs[token.i] = _sub_reflexive(token.text, gender)


def _sub_2nd_person_aux(verb):
    '''
    Args:
    (str) verb -- Replace this auxiliary verb with third person form.

    Returns:
    (str) Auxiliary verb inflected from second person to third person.
    '''
    sub = normalize(verb)
    if not is_modal(verb):
        sub = SECOND_PERSON_AUX_VERBS.get(sub, sub)
    sub = _add_caps_and_punc(verb, sub)
    return sub


def _sub_2nd_person_vbp(verb):
    '''
    Args:
    (str) verb -- Verb.

    Returns:
    (str) Verb inflected from second person to third person.
    '''
    sub = normalize(verb)
    if not is_modal(verb) and not is_2nd_person_aux(verb):
        for end in ['s', 'z', 'x', 'sh', 'ch']:
            if verb.endswith(end):
                if end == 'z':
                    if verb.endswith('iz'):
                        sub = verb + 'zes'
                    else:
                        sub = verb + 'es'
                else:
                    sub = verb + 'es'
                break
        else:
            if verb.endswith('o') and not verb.endswith('oo'):
                sub = verb + 'es'
            elif verb.endswith('y'):
                sub = verb.rstrip('y') + 'ies'
            else:
                sub = verb + 's'
    sub = _add_caps_and_punc(verb, sub)
    return sub


def _sub_subject(subject, gender):
    '''
    Args:
    (str) subject -- Subject.
    (int) gender -- Gender code.

    Returns:
    (str) Subject inflected from second person to third person.
    '''
    sub = normalize(subject)
    if sub == 'you':
        sub = GenderRules(gender).pronoun_subject
    sub = _add_caps_and_punc(subject, sub)
    return sub


def _sub_object(object_, gender):
    '''
    Args:
    (str) object_ -- Object.
    (int) gender -- Gender code.

    Returns:
    (str) Object inflected from second person to third person.
    '''
    sub = normalize(object_)
    if sub == 'you':
        sub = GenderRules(gender).pronoun_object
    sub = _add_caps_and_punc(object_, sub)
    return sub


def _sub_possessive(possessive, gender):
    '''
    Args:
    (str) possessive -- Possessive.
    (int) gender -- Gender code.

    Returns:
    (str) Possessive inflected from second person to third person.
    '''
    sub = normalize(possessive)
    if sub == 'your':
        sub = GenderRules(gender).possessive_adjective
    elif sub == 'yours':
        sub = GenderRules(gender).possessive_pronoun
    sub = _add_caps_and_punc(possessive, sub)
    return sub


def _sub_reflexive(pronoun, gender):
    '''
    Args:
    (str) pronoun -- Pronoun yourself.
    (int) gender -- Gender code.

    Returns:
    (str) Yourself inflected to himself or herself.
    '''
    sub = GenderRules(gender).pronoun_reflexive
    sub = _add_caps_and_punc(pronoun, sub)
    return sub


def _add_caps_and_punc(text, sub):
    '''
    Args:
    (str) text -- Original text.
    (sub) sub -- Replacement without capitalization or punctuations.

    Returns:
    (str) Replacement with appropriate capitalizations and
    punctuations.
    '''
    if text[0].isupper():
        sub = sub[0].upper() + sub[1:]
    if text[-1] in string.punctuation:
        sub = sub + text[-1]
    return sub
