"""
Analyze run times with various audio lengths.
"""

import os


def analyze():
    """
    Analyze audio of different lengths.
    """
    import psutil
    from pydub import AudioSegment

    from core.scribe import scribe
    from util.pathutil import AUDIO_DIR

    paths = [
        os.path.join(AUDIO_DIR, 'long-audio-0.wav'),
        os.path.join(AUDIO_DIR, 'long-audio-1.wav'),
        os.path.join(AUDIO_DIR, 'long-audio-2.wav'),
        os.path.join(AUDIO_DIR, 'long-audio-3.wav'),
        os.path.join(AUDIO_DIR, 'long-audio-4.wav'),
        os.path.join(AUDIO_DIR, 'long-audio-5.wav')
    ]

    process = psutil.Process(os.getpid())
    baseline_mem = process.memory_info().rss

    for path in paths:
        for _ in range(10):
            print(f'Audio time: {len(AudioSegment.from_wav(path)) / 1000}')
            start_mem = process.memory_info().rss
            with open(path, 'rb') as f:
                audio = f.read()
                scribe(audio, 1, 1, 'in-person', gcs_log=False)
            end_mem = process.memory_info().rss
            print(f'Memory usage: {baseline_mem + end_mem - start_mem}')


def organize():
    """
    Organize data into CSV file.
    """
    from pandas import DataFrame
    from util.pathutil import DATA_DIR

    in_path = os.path.join(DATA_DIR, 'long-audio-times.txt')
    out_time_path = os.path.join(DATA_DIR, 'long-audio-times.csv')
    out_memory_path = os.path.join(DATA_DIR, 'long-audio-memory.csv')

    all_time_data = []
    sum_time = 0
    all_memory_data = []
    with open(in_path, 'r') as f:
        for line in f.read().splitlines():
            parts = line.split(': ')
            label = parts[0]
            value = float(parts[1])
            if label == 'Audio time':
                audio_time = round(value / 60)
            if label not in ['Audio time', 'Note time 1', 'Memory usage']:
                sum_time += value
                if label == 'Upload time':
                    label = 'Cloud Upload'
                if label == 'Transcribe time':
                    label = 'Transcription'
                if label == 'Note time 2':
                    label = 'Note Creation'
                all_time_data.append([audio_time, label, value])
            if label == 'Memory usage':
                value /= 1e9
                all_memory_data.append([audio_time, 'memory', value])
                all_time_data.append([audio_time, 'Cumulative', sum_time])
                sum_time = 0

    df_time = DataFrame(all_time_data, columns=['audio_time', 'measurement', 'value'])
    df_time.to_csv(out_time_path)

    df_memory = DataFrame(all_memory_data, columns=['audio_time', 'measurement', 'value'])
    df_memory.to_csv(out_memory_path)
