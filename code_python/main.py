'''
Set up environment and execute code.
'''

import os
import sys

# Set up environment.
sys.path.insert(0, os.path.dirname(__file__))

if __name__ == '__main__':
    from core.scribe import scribe
    audio = open('/home/jessewang/data/test/<filename>', 'rb').read()
    note, transcript = scribe(audio, 1, 1)
    print(note.to_json())
    print(transcript)
