'''
Flask web server.
'''

import os
import re
import json
import traceback

from collections import namedtuple
from math import ceil

from datetime import datetime
from flask import (
    Flask,
    render_template,
    request,
    redirect,
    url_for,
    jsonify,
    make_response
)
from flask_login import (
    LoginManager,
    login_user,
    UserMixin,
    logout_user,
    current_user,
    login_required
)
from flask_mail import Mail, Message
from flask_talisman import Talisman
from flask_wtf.csrf import CSRFProtect, CSRFError
from passlib.hash import pbkdf2_sha512
from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from pymongo import MongoClient
from gridfs import GridFS
from celery import Celery
from bson.json_util import dumps as bson_to_json
from bson.objectid import ObjectId
from uuid import uuid4
from pydub import AudioSegment

from core.scribe import scribe
from core.section import EXAM_CUE, ANP_CUE
from util.pathutil import CONF_DIR

# Flask app.
app = Flask(__name__)
app.config.from_pyfile(os.path.join(CONF_DIR, 'flask.cfg'))

# Login manager instance.
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

# Serializer.
serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])

# Token salts.
salt_account_confirm = 'salt_account_confirm'
salt_password_change = 'salt_password_change'

# Flask mail.
mail = Mail(app)

# Allow content from these domains.
csp = {
    'default-src': [
        "'self'",
        'blob:'
    ],
    'script-src': [
        "'self'",
        'https://code.jquery.com/jquery-3.3.1.slim.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js',
        'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js',
        'https://unpkg.com/hotkeys-js/dist/hotkeys.min.js',
        'https://unpkg.com/peerjs@1.3.0/dist/peerjs.min.js'
    ],
    'connect-src': [
        "'self'",
        'https://0.peerjs.com/peerjs/id',
        'https://digitalscribe.xyz:9002/peerjs/id',
        'wss://digitalscribe.xyz:9002/peerjs'
    ],
    'style-src': [
        "'self'",
        "'unsafe-inline'",
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
        'https://use.fontawesome.com/releases/v5.8.2/css/all.css',
        'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css'
    ],
    'worker-src': [
        "'self'",
        'blob:'
    ],
    'img-src': [
        "'self'",
        'data:',
    ],
    'media-src': [
        "'self'",
        'blob:'
    ],
    'font-src': '*',
    'frame-src': [
        "'self'",
        'https://www.youtube.com/'
    ]
}

# Add Talisman security.
Talisman(app, content_security_policy=csp)

# Anti-CSRF (Cross-Site Request Forgery)
csrf = CSRFProtect()
csrf.init_app(app)

# App database.
mongo_db = MongoClient(connect=False)['penq']

# GridFS for inserting large files into app database.
mongo_fs = GridFS(mongo_db)

# Celery for asynchronous tasks.
celery = Celery(
    app.name,
    broker=app.config['CELERY_BROKER_URL'],
    backend=app.config['CELERY_RESULT_BACKEND'])


class User(UserMixin):
    '''
    Represents users for login sessions.
    '''
    def __init__(self, cookie_id, email):
        '''
        Args:
        (str) cookie_id -- Cookie ID for login.
        (str) email -- Email.

        Attrs:
        (str) _cookie_id -- Cookie ID for login.
        (str) _email -- Email.
        '''
        self._cookie_id = cookie_id
        self._email = email

    def get_id(self):
        '''
        Returns a unicode that uniquely identifies this user, and can
        be used to load the user from the user_loader callback. Note
        that this must be a unicode.
        '''
        return self._cookie_id

    @property
    def email(self):
        return self._email

    @staticmethod
    def get_cookie_id(email):
        return f'{email}_{uuid4().hex}'

    @staticmethod
    def get_email_from_cookie_id(cookie_id):
        return cookie_id.split('_')[0]


@login_manager.user_loader
def load_user(cookie_id):
    '''
    Get user object.
    '''
    email = User.get_email_from_cookie_id(cookie_id)
    pydoc = mongo_db['users'].find_one({'_id': email})
    if pydoc is not None and cookie_id == pydoc['cookie_id']:
        return User(pydoc['cookie_id'], email)
    else:
        return None


@app.route('/')
def home():
    '''
    Home page.
    '''
    if not current_user.is_active:
        return render_template('home.html')
    else:
        return redirect(url_for('notes', page=1))


@app.route('/message/<text_title>/<text_type>/<text>')
def message(text_title, text_type, text):
    '''
    Message page.
    '''
    return render_template(
        'message.html', text_title=text_title, text_type=text_type, text=text)


@app.route('/waiting-room')
def waiting_room():
    '''
    Waiting room for voice chats.
    '''
    return render_template('waiting-room.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    '''
    Login page. If user exists and is verified, then return message
    saying account already exists. If user exists and is not verified,
    then email another confirmation link and return message saying
    account is pending. If user does not exist, then return message
    saying so.
    '''
    if request.method == 'GET':
        if current_user.is_active:
            return redirect(url_for('notes', page=1))
        else:
            return render_template('login.html')
    else:
        email = request.form['email']
        store = request.form.get('remember-me')
        users = mongo_db['users']
        pydoc = users.find_one({'_id': email})
        if pydoc:
            if pbkdf2_sha512.verify(request.form['password'], pydoc['password']):
                if pydoc['checked_on']:
                    user = User(pydoc['cookie_id'], email)
                    if store and store == 'on':
                        login_user(user, remember=True)
                    else:
                        login_user(user, remember=False)
                    return redirect(url_for('notes', page=1))
                else:
                    send_confirmation(email)
                    return redirect(url_for(
                        'message',
                        text_title='Account Exists',
                        text_type='alert-danger',
                        text='A confirmed account already exists.'))
        return render_template('login.html', error='Invalid login credentials.')


@app.route('/logout', methods=['GET'])
def logout():
    '''
    Route for logging out users.
    '''
    logout_user()
    return redirect(url_for('home'))


@app.route('/account-create', methods=['GET', 'POST'])
def account_create():
    '''
    Create account page. If user already exists in the database and is
    verified, then return the create account page with an error
    message. If user exists in the database and is unverified, then
    email a confirmation link. If user does not exist in the database,
    then add user to the database and email a confirmation link.
    '''
    if request.method == 'GET':
        return render_template('account-create.html')
    else:
        email = request.form['email']
        password = pbkdf2_sha512.hash(request.form['password'])
        users = mongo_db['users']
        pydoc = users.find_one({'_id': email})
        if pydoc:
            if pydoc['checked_on']:
                return redirect(url_for(
                    'message',
                    text_title='Account Exists',
                    text_type='alert-danger',
                    text='A confirmed account already exists.'))
            else:
                send_confirmation(email)
                return redirect(url_for(
                    'message',
                    text_title='Account Pending',
                    text_type='alert-warning',
                    text=('A pending account already exists. '
                        'Please check your email.')))
        else:
            users.insert({
                '_id': email,
                'cookie_id': User.get_cookie_id(email),
                'notes': [],
                'password': password,
                'created_on': datetime.utcnow(),
                'checked_on': None
            })
            send_confirmation(email)
            return redirect(url_for(
                'message',
                text_title='Account Requested',
                text_type='alert-warning',
                text='We emailed you a confirmation link.'))


@app.route('/account-confirmed/<token>')
def account_confirmed(token):
    '''
    Account confirmed page. If token is valid, then update user
    database entry with time of verification and return a notification
    confirmed page. If token is expired, then send another confirmation
    email and return an appropriate notfiation page. If token is
    corrupted or any other problem, return a generic notification error
    page.
    '''
    try:
        email = serializer.loads(token, salt=salt_account_confirm, max_age=3600)
        users = mongo_db['users']
        users.update_one(
            {'_id': email},
            {'$set': {'checked_on': datetime.utcnow()}})
        return redirect(url_for(
            'message',
            text_title='Account Confirmed',
            text_type='alert-success',
            text='Your account is confirmed. Thank you!'))
    except SignatureExpired as e:
        if e.payload:
            try:
                email = serializer.load_payload(e.payload)
                send_confirmation(email)
                return redirect(url_for(
                    'message',
                    text_title='Account Pending',
                    text_type='alert-warning',
                    text=('Your confirmation link expired. '
                        'We emailed you another one.')))
            except:
                return redirect(url_for('uh_oh'))
        else:
            return redirect(url_for('uh_oh'))
    except:
        return redirect(url_for('uh_oh'))


@app.route('/account-password-change', methods=['GET', 'POST'])
@login_required
def account_password_change():
    '''
    Route for changing password when logged in.
    '''
    if request.method == 'GET':
        return render_template('account-password-change.html')
    else:
        users = mongo_db['users']
        hash_old_password = users.find_one(
            {'_id': current_user.email}, {'password': 1})['password']
        form_old_password = request.form['old-password']
        hash_new_password = pbkdf2_sha512.hash(request.form['new-password'])
        if pbkdf2_sha512.verify(form_old_password, hash_old_password):
            new_cookie_id = User.get_cookie_id(current_user.email)
            users.update_one(
                {'_id': current_user.email},
                {'$set': {'password': hash_new_password, 'cookie_id': new_cookie_id}})
            return redirect(url_for(
                'message',
                text_title='Password Changed',
                text_type='alert-success',
                text='Password successfully changed.'))
        else:
            return render_template(
                'account-password-change.html', error='Incorrect password.')


@app.route('/account-delete', methods=['GET', 'POST'])
@login_required
def account_delete():
    '''
    Route for deleting account.
    '''
    if request.method == 'GET':
        return render_template('account-delete.html')
    else:
        user = mongo_db['users'].find_one(
            {'_id': current_user.email}, {'password': 1, 'notes': 1})
        if pbkdf2_sha512.verify(request.form['password'], user['password']):
            logout_user()
            notes = mongo_db['notes'].find({'_id': {'$in': user['notes']}})
            for note in notes:
                mongo_fs.delete(note['audio_id'])
                mongo_db['notes'].delete_one({'_id': note['_id']})
            mongo_db['users'].delete_one({'_id': user['_id']})
            return redirect(url_for(
                'message',
                text_title='Account Deleted',
                text_type='alert-success',
                text='Account successfully deleted.'))
        else:
            return render_template(
                'account-delete.html', error='Incorrect password.')


@app.route('/password-forgot', methods=['GET', 'POST'])
def password_forgot():
    '''
    Forgot password page. Let user enter an email address. If email is
    associated with a verified account, then email a link for changing
    the password. If email is associated with an unverified account,
    then email a link for verifying the account. If email is not
    associated with an account, then redirect to a notification account
    absent page.
    '''
    if request.method == 'GET':
        return render_template('password-forgot.html')
    else:
        email = request.form['email']
        users = mongo_db['users']
        pydoc = users.find_one({'_id': email})
        if pydoc:
            if pydoc['checked_on']:
                send_password_change(email)
                return redirect(url_for(
                    'message',
                    text_title='Password Reset',
                    text_type='alert-warning',
                    text='We emailed you a password reset link.'))
            else:
                send_confirmation(email)
                return redirect(url_for(
                    'message',
                    text_title='Account Pending',
                    text_type='alert-warning',
                    text=('A pending account already exists. '
                        'Please check your email.')))
        else:
            return redirect(url_for(
                'message',
                text_title='No Account',
                text_type='alert-danger',
                text='No account found.'))


@app.route('/password-change/<token>', methods=['GET', 'POST'])
def password_change(token):
    '''
    Password change page.
    '''
    try:
        email = serializer.loads(token, salt=salt_password_change, max_age=1800)
        if request.method == 'GET':
            return render_template('password-change.html', token=token, email=email)
        else:
            password = pbkdf2_sha512.hash(request.form['password'])
            users = mongo_db['users']
            users.update_one(
                {'_id': email},
                {'$set': {'password': password, 'cookie_id': User.get_cookie_id(email)}})
            return redirect(url_for(
                'message',
                text_title='Password Changed',
                text_type='alert-success',
                text='Password successfully changed.'))
    except SignatureExpired as e:
        if e.payload:
            try:
                email = serializer.load_payload(e.payload)
                return redirect(url_for(
                    'message',
                    text_title='Password Reset',
                    text_type='alert-warning',
                    text='Password reset link expired.'))
            except:
                return redirect(url_for('uh_oh'))
        else:
            return redirect(url_for('uh_oh'))
    except:
        return redirect(url_for('uh_oh'))


@app.route('/account', methods=['GET'])
def account():
    '''
    Route for account settings.
    '''
    return render_template('account.html')


@app.route('/notes/<page>', methods=['GET'])
@login_required
def notes(page):
    '''
    Notes page.
    '''
    size = 10
    page = int(page)
    user = mongo_db['users'].find_one(
        {'_id': current_user.email}, {'notes': 1})
    notes = list(mongo_db['notes'].find(
        {'_id': {'$in': user['notes']}}, 
        {'name': 1, 'datetime': 1, 'status': 1}))
    notes.reverse()
    init = size * (page - 1)
    stop = size * page
    page_notes = notes[init : stop]
    for index, note in enumerate(page_notes):
        note['index'] = index
        note['name'] = note['name'][0:20] if note['name'] else 'No Name'
        note['datetime'] = note['datetime'].isoformat() + 'Z'
    has_prev_link = False if page <= 1 else True
    has_next_link = False if stop >= len(notes) else True
    page_links, prev_link, next_link = pagination(size, page, len(notes))
    return render_template(
        'notes.html',
        notes=page_notes,
        has_prev_link=has_prev_link,
        has_next_link=has_next_link,
        prev_link=prev_link,
        next_link=next_link,
        page_links=page_links)


def pagination(size, page, total_note_count):
    '''
    Get values for pagination.
    '''
    page_links = []
    page_link = namedtuple('page_link', ['url', 'page', 'active'])
    lim_page = ceil(total_note_count / size)
    if page < 7:
        min_page = 1
        max_page = min(lim_page, 10)
    else:
        min_page = page - 5
        max_page = min(lim_page, page + 4)
    for x in range(min_page, max_page + 1):
        url = url_for('notes', page=x)
        active = 'active' if page == x else ''
        page_links.append(page_link(url, x, active))
    prev_link = page_link(url_for('notes', page=page-1), '', '')
    next_link = page_link(url_for('notes', page=page+1), '', '')
    return page_links, prev_link, next_link


@app.route('/refresh-notes', methods=['POST'])
@login_required
def refresh_notes():
    '''
    Refresh notes page.
    '''
    str_ids = request.form.get('note-ids', type=str).split(',')
    obj_ids = [ObjectId(str_id) for str_id in str_ids]
    notes = list(mongo_db['notes'].find(
        {'_id': {'$in': obj_ids}}, {'user': 1, 'status': 1}))
    for note in notes:
        if current_user.email != note['user']:
            return redirect(url_for('uh_oh'))
        note['_id'] = str(note['_id'])
        note.pop('user')
    return jsonify(notes)


@app.route('/record', methods=['GET'])
@login_required
def record():
    '''
    Record page.
    '''
    return render_template('record.html')


@app.route('/help', methods=['GET'])
@login_required
def help():
    '''
    Help page.
    '''
    return render_template('help.html')


@app.route('/settings', methods=['GET'])
@login_required
def settings():
    '''
    Settings page.
    '''
    return render_template('settings.html')


@app.route('/audio', methods=['POST'])
@login_required
def audio():
    '''
    Route for submitting audio.
    '''
    encounter_type = request.form.get('encounter-type', type=str)
    if encounter_type == 'in-person':
        audio = request.files['audio']
    elif encounter_type == 'telemedicine':
        local_audio_filename = f'{uuid4().hex}.wav'
        remote_audio_filename = f'{uuid4().hex}.wav'
        merged_audio_filename = f'{uuid4().hex}.wav'
        request.files['local-audio'].save(local_audio_filename)
        request.files['remote-audio'].save(remote_audio_filename)
        local_audio = AudioSegment.from_wav(local_audio_filename)
        remote_audio = AudioSegment.from_wav(remote_audio_filename)[:len(local_audio)]
        local_audio = local_audio[:len(remote_audio)]
        merged_audio = AudioSegment.from_mono_audiosegments(local_audio, remote_audio)
        merged_audio.export(merged_audio_filename, format='wav')
        with open(merged_audio_filename, 'rb') as f:
            audio = f.read()
        os.remove(local_audio_filename)
        os.remove(remote_audio_filename)
        os.remove(merged_audio_filename)
    caseFile = request.files.get('case-template', None)
    if caseFile:
        caseFile.seek(0)
        caseFileContent = caseFile.read()
    else:
        caseFileContent = None
    people = request.form.get('people', type=int)
    pronoun = request.form.get('pronoun', type=int)
    name = ' '.join(request.form.get('input-name', type=str).split())
    users = mongo_db['users']
    notes = mongo_db['notes']
    audio_id = mongo_fs.put(audio)
    note_id = notes.insert_one({
        'user': current_user.email,
        'case_template': caseFileContent,
        'people': people,
        'pronoun': pronoun,
        'audio_id': audio_id,
        'name': name,
        'status': 'pending',
        'datetime': datetime.utcnow(),
        'json': None,
        'transcript': None,
        'markup_sub': None,
        'markup_obj': None,
        'markup_anp': None,
        'time_sub': 0,
        'time_obj': 0,
        'time_anp': 0,
        'dictation_sub': [],
        'dictation_obj': [],
        'dictation_anp': [],
        'editable': True,
        'encounter_type': encounter_type
    }).inserted_id
    users.update_one(
        {'_id': current_user.email},
        {'$push': {'notes': note_id}})
    create_note.delay(str(note_id))
    return 'Audio uploaded.'


@app.route('/playback/<note_id>', methods=['GET'])
@login_required
def playback(note_id):
    '''
    Route for streaming audio playback.
    '''
    obj_id = ObjectId(note_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id}, {'user': 1, 'audio_id': 1})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    file = mongo_fs.get(note['audio_id'])
    response = make_response(file.read())
    response.mimetype = 'audio/wav'
    return response


@app.route('/view/<note_id>', methods=['GET'])
@login_required
def view(note_id):
    '''
    Route for editing note.
    '''
    obj_id = ObjectId(note_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    name = note['name'][0:20] if note['name'] else 'No Name'
    return render_template(
        'view.html',
        name=name,
        note_id=note_id,
        markup_sub=note['markup_sub'],
        markup_obj=note['markup_obj'],
        markup_anp=note['markup_anp'],
        editable=note['editable'])


@app.route('/save', methods=['POST'])
@login_required
def save():
    '''
    Route for saving note.
    '''
    str_id = request.form.get('note-id', type=str)
    obj_id = ObjectId(str_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id}, {'user': 1, 'editable': 1})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    if note['editable']:
        markup = request.form.get('markup', type=str)
        section = request.form.get('section', type=str)
        notes.update_one(
            {'_id': obj_id}, {'$set': {f'markup_{section}': markup}})
        return 'Note saved.'
    else:
        return 'Note is not editable.'


@app.route('/lock', methods=['POST'])
@login_required
def lock():
    '''
    Route for locking note.
    '''
    str_id = request.form.get('note-id', type=str)
    obj_id = ObjectId(str_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id}, {'user': 1, 'editable': 1})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    if note['editable']:
        notes.update_one({'_id': obj_id}, {'$set': {'editable': False}})
        return 'Note locked.'
    else:
        return 'Note is not editable.'


@app.route('/stopwatch', methods=['POST'])
@login_required
def stopwatch():
    '''
    Route for saving time editing note.
    '''
    str_id = request.form.get('note-id', type=str)
    obj_id = ObjectId(str_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id}, {'user': 1, 'editable': 1})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    if note['editable']:
        section = request.form.get('section', type=str)
        interval = request.form.get('interval', type=int)
        notes.update_one(
            {'_id': obj_id}, {'$inc': {f'time_{section}': interval}})
        return 'Interval added.'
    else:
        return 'Note is not editable.'


@app.route('/dictation', methods=['POST'])
@login_required
def dictation():
    '''
    Route for saving dictations while editing note.
    '''
    str_id = request.form.get('note-id', type=str)
    obj_id = ObjectId(str_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id}, {'user': 1})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    section = request.form.get('section', type=str)
    transcript = request.form.get('transcript', type=str)
    notes.update_one({'_id': obj_id}, {'$push': {f'dictation_{section}': transcript}})
    return 'Dictation added.'


@app.route('/trash', methods=['POST'])
@login_required
def trash():
    '''
    Route for deleting note.
    '''
    str_ids = request.form.get('note-ids', type=str)
    notes = mongo_db['notes']
    users = mongo_db['users']
    for str_id in str_ids.split('|'):
        obj_id = ObjectId(str_id)
        note = notes.find_one({'_id': obj_id})
        if current_user.email != note['user']:
            return redirect(url_for('uh_oh'))
        mongo_fs.delete(note['audio_id'])
        notes.delete_one({'_id': obj_id})
        users.update_one(
            {'_id': current_user.email},
            {'$pull': {'notes': obj_id}}
        )
    return 'Note deleted.'


@app.route('/note-data/<note_id>', methods=['GET'])
@login_required
def note_data(note_id):
    '''
    Route for getting note data in JSON.
    '''
    obj_id = ObjectId(note_id)
    notes = mongo_db['notes']
    note = notes.find_one({'_id': obj_id})
    if current_user.email != note['user']:
        return redirect(url_for('uh_oh'))
    dump = bson_to_json(note, indent=2)
    return dump


@app.after_request
def after_request(response):
    '''
    Modify all responses.
    '''
    response.headers.add('Accept-Ranges', 'bytes')
    return response


@celery.task(ignore_result=True)
def create_note(str_id):
    '''
    Convert audio recording into a note.
    '''
    obj_id = ObjectId(str_id)
    notes = mongo_db['notes']
    try:
        pydoc = notes.find_one({'_id': obj_id})
        audio = mongo_fs.get(pydoc['audio_id']).read()
        try:
            template = json.loads(pydoc['case_template'])
        except:
            template = {}
            pass
        note, transcript = scribe(
            audio, 
            pydoc['people'],
            pydoc['pronoun'],
            pydoc['encounter_type'],
            gcs_log=False, 
            template=template)
        if note:
            dump = note.to_json()
            markup_sub, markup_obj, markup_anp = get_markup(dump)
            notes.update_one(
                {'_id': obj_id},
                {'$set': {
                    'status': 'ready',
                    'json': dump,
                    'transcript': transcript,
                    'markup_sub': markup_sub,
                    'markup_obj': markup_obj,
                    'markup_anp': markup_anp}})
        else:
            notes.update_one(
                {'_id': obj_id},
                {'$set': {
                    'status': 'failed',
                    'markup_sub': '',
                    'markup_obj': '',
                    'markup_anp': ''}})
    except:
        traceback.print_exc()
        notes.update_one({'_id': obj_id}, {'$set': {'status': 'failed'}})


def get_markup(dump):
    '''
    Convert note history JSON to HTML.
    '''
    sub = ''
    obj = ''
    anp = ''
    if dump:
        note = json.loads(dump)
        for section in note:
            head = section['head'].upper()
            if section['head'].lower() == EXAM_CUE.lower():
                obj += head + '\r\n\r\n'
                if isinstance(section['body'], list):
                    for subsection in section['body']:
                        if subsection['head']:
                            obj += subsection['head'] + ':\n'
                        if subsection['body']:
                            obj += subsection['body'] + '\n\n'
                        else:
                            obj += '\r\n\r\n'
                else:
                    obj += '\r\n\r\n'
            elif section['head'].lower() == ANP_CUE.lower():
                anp += head + '\r\n'
                if section['body']:
                    anp += section['body'] + '\n\n'
                else:
                    anp += '\r\n\r\n'
            else:
                sub += head + '\r\n'
                if section['body']:
                    if isinstance(section['body'], list):
                        for item in section['body']:
                            sub += '-' + item + '\n'
                    else:
                        sub += section['body'] + '\n\n'
                else:
                    sub += '\n\n'
    return sub, obj, anp


@celery.task(ignore_result=True)
def send_email(data):
    '''
    Send email.
    '''
    with app.app_context():
        message = Message(data['subject'], recipients=data['recipients'])
        message.body = data['body']
        mail.send(message)


@app.route('/uh-oh')
def uh_oh():
    '''
    Uh oh page. A generic notification page for unexpected errors.
    '''
    return render_template('uh-oh.html')


@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    '''
    Route for CSRF errors.
    '''
    return redirect(url_for('uh_oh'))


def send_confirmation(email):
    '''
    Send account confirmation email.
    '''
    token = serializer.dumps(email, salt=salt_account_confirm)
    link = url_for('account_confirmed', token=token, _external=True)
    subject = 'Digital Scribe: Account Confirmation'
    recipients = [email]
    body = (
        'Hello,\n\n'
        'Welcome to the Digital Scribe! Please click this link to '
        f'confirm your account: {link}.\n\n'
        'Thanks,\n'
        'The Digital Scribe Team')
    send_email.delay({
        'subject': subject,
        'recipients': recipients,
        'body': body
    })


def send_password_change(email):
    '''
    Email a link for changing the password.
    '''
    token = serializer.dumps(email, salt=salt_password_change)
    link = url_for('password_change', token=token, _external=True)
    subject = 'Digital Scribe: Change Password'
    recipients = [email]
    body = (
        'Hello,\n\n'
        f'Here is your password reset link: {link}.\n\n'
        'Thanks,\n'
        'The Digital Scribe Team')
    send_email.delay({
        'subject': subject,
        'recipients': recipients,
        'body': body
    })


def run():
    '''
    Run server in development mode.
    '''
    app.run(host='0.0.0.0', debug=True)
