const trimSpace = text => {
	if (text.charAt(0) === ' ') {
		text = text.slice(1, text.length);
	}
	if (text.charAt(text.length - 1) === ' ') {
		text = text.slice(0, text.length - 1);
	}
	return text;
};

const trimPunctuations = text => {
	text = trimSpace(text);
	for (const char of '.,!?') {
		if (text.endsWith(char)) {
			text = text.slice(0, text.length - 1);
		}
		if (text.startsWith(char)) {
			text = text.slice(1, text.length);
		}
	}
	text = trimSpace(text);
	return text;
};

const uppercaseFirstLetter = text => {
	return text.charAt(0).toUpperCase() + text.slice(1, text.length);
};

const lowercaseFirstLetter = text => {
	return text.charAt(0).toLowerCase() + text.slice(1, text.length);
};

const editPunctuation = (textarea, action) => {
	const text = textarea.value;
	const start = textarea.selectionStart;
	const end = textarea.selectionEnd;
	let half1 = trimPunctuations(text.slice(0, start));
	let half2 = trimPunctuations(text.slice(end, text.length));
	switch (action) {
		case 'period':
			half1 += '. '
			half2 = uppercaseFirstLetter(half2);
			break;
		case 'comma':
			half1 += ', '
			half2 = lowercaseFirstLetter(half2);
			break;
		case 'delete':
			half1 += ' '
			half2 = lowercaseFirstLetter(half2);
			break;
		default:
			console.log('Invalid edit action.');
			break;
	}
	textarea.value = half1 + half2;
	textarea.focus({ preventScroll: true });
	textarea.selectionStart = half1.length;
	textarea.selectionEnd = half1.length;
};