$(document).ready(() => {
	const main = document.getElementById('main');
	const noteID = document.getElementById('note-id');
	const csrfToken = document.getElementById('csrf_token');
	const saveURL = document.getElementById('save-url');
	const saveBtn = document.getElementById('btn-save');
	const saveIcon = document.getElementById('icon-save');
	const navNotes = document.getElementById('nav-notes');
	const section = document.getElementById('section');
	const stopwatchURL = document.getElementById('stopwatch-url');
	const btnSub = document.getElementById('btn-sub');
	const btnObj = document.getElementById('btn-obj');
	const btnAnp = document.getElementById('btn-anp');
	const sectionItems = document.getElementsByClassName('section-item');
	const btnDictate = document.getElementById('btn-dictate');
	const recordingIcon = document.getElementById('recording-icon');
	const dictationURL = document.getElementById('dictation-url');
	const editableElem = document.getElementById('editable');
	const btnLock = document.getElementById('btn-lock');
	const lockURL = document.getElementById('lock-url');
	const btnData = document.getElementById('btn-data');
	const dataURL = document.getElementById('data-url');

	let editable = (editableElem.content.toLowerCase() === 'true');

	navNotes.classList.add('active');
	btnSub.classList.add('active');

	if (!editable) {
		saveBtn.disabled = true;
		btnDictate.disabled = true;
		btnLock.disabled = true;
	}

	let currentEditor = null;
	let snapshot = null;
	let timeout = null;

	const getActiveSection = () => {
		if (btnSub.classList.contains('active')) {
			return 'sub';
		}
		else if (btnObj.classList.contains('active')) {
			return 'obj';
		}
		else if (btnAnp.classList.contains('active')) {
			return 'anp';
		}
	};

	const deactivateSectionItems = () => {
		for (let i = 0; i < sectionItems.length; i++) {
			sectionItems[i].classList.remove('active');
		}
		if (currentEditor !== null) {
			currentEditor.summernote('destroy');
		}
	};

	const setActiveSection = selected => {
		deactivateSectionItems();
		selected.classList.add('active');
		activeSection = getActiveSection();
		currentEditor = $('#editor-' + activeSection);
		currentEditor.summernote({
			tabsize: 2,
			height: 450,
			disableResizeEditor: true,
			toolbar: [
				['undo', ['undo']],
				['redo', ['redo']],
				['style', ['bold', 'italic', 'underline', 'clear']],
				['para', ['ul', 'ol', 'paragraph']],
			],
			followingToolbar: false
		});
		currentEditor.on('summernote.change', () => {
			if (timeout) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(() => {
				if (snapshot !== currentEditor.summernote('code')) {
					save();
				}
			}, 1000);
		});
		snapshot = currentEditor.summernote('code');
		if (!editable) {
			currentEditor.summernote('disable');
		}
	};

	setActiveSection(btnSub);

	const save = () => {
		saveIcon.classList.remove('fa-save');
		saveIcon.classList.add('fa-spinner');
		const markupEdit = currentEditor.summernote('code');
		snapshot = markupEdit;
		const xhr = new XMLHttpRequest();
		xhr.onload = () => {
			if (xhr.readyState === 4) {
				setTimeout(() => {
					saveIcon.classList.remove('fa-spinner');
					saveIcon.classList.add('fa-save');
				}, 500);
			}
		};
		const formData = new FormData();
		formData.append('note-id', noteID.content);
		formData.append('markup', markupEdit);
		formData.append('section', getActiveSection());
		xhr.open('POST', saveURL.content, true);
		xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
		xhr.send(formData);
	};

	for (let i = 0; i < sectionItems.length; i++) {
		const elem = sectionItems[i];
		elem.addEventListener('click', () => {
			if (snapshot !== currentEditor.summernote('code')) {
				save();
			}
			setActiveSection(elem);
		});
	}

	saveBtn.addEventListener('click', () => {
		save();
	});

	document.addEventListener('keydown', (e) => {
		if ((e.ctrlKey || e.metaKey) && e.which === 83) {
			e.preventDefault();
			save();
		}
	});

	main.style.visibility = 'visible';

	setInterval(() => {
		if (editable) {
			const xhr = new XMLHttpRequest();
			const formData = new FormData();
			formData.append('note-id', noteID.content);
			formData.append('section', getActiveSection());
			formData.append('interval', 1);
			xhr.open('POST', stopwatchURL.content, true);
			xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
			xhr.send(formData);
		}
	}, 1000);

	let final_transcript = '';
	let recognizing = false;
	let ignore_onend;
	let start_timestamp;

	if (!('webkitSpeechRecognition' in window)) {
		upgrade();
	} else {
		const recognition = new webkitSpeechRecognition();
		recognition.continuous = true;
		recognition.interimResults = true;

		recognition.onstart = () => {
			recognizing = true;
			console.log('info_speak_now');
		};

		recognition.onerror = (event) => {
			if (event.error === 'no-speech') {
				console.log('info_no_speech');
				ignore_onend = true;
			}
			if (event.error === 'audio-capture') {
				console.log('info_no_microphone');
				ignore_onend = true;
			}
			if (event.error === 'not-allowed') {
				if (event.timeStamp - start_timestamp < 100) {
					console.log('info_blocked');
				} else {
					console.log('info_denied');
				}
				ignore_onend = true;
			}
		};

		recognition.onend = () => {
			recognizing = false;
			if (ignore_onend) {
				return;
			}
			if (!final_transcript) {
				console.log('info_start');
				return;
			}
		};

		const capitalize = (s) => {
			const sentences = s.split('. ');
			for (let i = 0; i < sentences.length; i++) {
				sentences[i] = sentences[i].replace(/\S/, function(m) { return m.toUpperCase(); });
			}
			return sentences.join('. ');
		};

		const saveTranscript = (transcript) => {
			const xhr = new XMLHttpRequest();
			const formData = new FormData();
			formData.append('note-id', noteID.content);
			formData.append('section', getActiveSection());
			formData.append('transcript', transcript);
			xhr.open('POST', dictationURL.content, true);
			xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
			xhr.send(formData);
		};

		recognition.onresult = (event) => {
			for (let i = event.resultIndex; i < event.results.length; i++) {
				if (event.results[i].isFinal) {
					const transcript = capitalize(event.results[i][0].transcript);
					currentEditor.summernote('editor.insertText', transcript);
					saveTranscript(transcript);
				}
			}
		};

		btnDictate.addEventListener('click', () => {
			if (recognizing) {
				recognition.stop();
				recordingIcon.style.visibility = 'hidden';
				return;
			}
			recognition.start();
			recordingIcon.style.visibility = 'visible';
		});

		btnData.addEventListener('click', () => {
			const a = document.createElement('a');
			a.download = 'note-data.json';
			a.href = dataURL.content;
			a.click();
		});

		btnLock.addEventListener('click', () => {
			const message = "Are you sure? Locking will prevent future editing.";
			if (!confirm(message)) {
				return;
			}
			save();
			const xhr = new XMLHttpRequest();
			const formData = new FormData();
			formData.append('note-id', noteID.content);
			xhr.open('POST', lockURL.content, true);
			xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
			xhr.send(formData);
			editable = false;
			currentEditor.summernote('disable');
			saveBtn.disabled = true;
			btnDictate.disabled = true;
			btnLock.disabled = true;
			if (recognizing) {
				recognition.stop();
				recordingIcon.style.visibility = 'hidden';
			}
		});
	}

});

