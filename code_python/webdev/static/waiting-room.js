const navRoom = document.getElementById('nav-room');
const peerId = document.getElementById('peer-id');
const message = document.getElementById('message');
const player = document.getElementById('player')

navRoom.classList.add('active');

const peer = new Peer({ 
	host: 'digitalscribe.xyz', 
	port: 9002, 
	path: '/', 
	secure: true });

peer.on('open', id => {
	peerId.innerText = 'ID: ' + id;
	message.innerText = 'Waiting for call...'
});

peer.on('connection', conn => {
	conn.on('data', data => {
		console.log(data);
	});
});

peer.on('call', call => {
	navigator.mediaDevices.getUserMedia({ audio: true }).then(localStream => {
		call.answer(localStream);
		call.on('stream', remoteStream => {
			player.srcObject = remoteStream;
			player.play();
			message.innerText = 'Connected.'
		});
	});
});
