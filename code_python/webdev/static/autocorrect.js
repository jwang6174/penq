const punctuations = '.,!?-:;';


const autocorrectCapitalizations = (text) => {
	const buildLines = [];
	const lines = text.split('\n');
	for (let j = 0; j < lines.length; j++) {
		const line = lines[j];
		const build = [];
		let lastPunctuationShouldCap = false;
		for (let i = 0; i < line.length; i++) {
			if (i + 1 < line.length - 1 && punctuations.includes(line[i]) && line[i + 1] == ' ') {
				if (line[i] === ',') {
					lastPunctuationShouldCap = false;
				}
				else {
					lastPunctuationShouldCap = true;
				}
				build.push(line[i]);
			}
			else if (i - 1 >= 0 && line[i] !== ' ' && line[i - 1] === ' ') {
				if (lastPunctuationShouldCap) {
					build.push(line[i].toUpperCase());
					lastPunctuationShouldCap = false;
				}
				else if (!hasMultipleCaps(getWord(i, line))) {
					build.push(line[i].toLowerCase());
				}
				else {
					build.push(line[i]);
				}
			}
			else if (i === 0) {
				build.push(line[i].toUpperCase());
			}
			else {
				build.push(line[i]);
			}
		}
		buildLines.push(build.join(''));
	}
	return buildLines.join('\n');
};


const getWord = (charIndex, text) => {
	const chars = [];
	for (let i = charIndex; i < text.length; i++) {
		if (text[i] !== ' ') {
			chars.push(text[i]);
		}
		else {
			break;
		}
	}
	return chars.join('');
};


const getLastWord = (charIndex, text) => {
	const words = text.split(' ');
	let count = 0;
	for (let i = 1; i < words.length; i++) {
		count += words[i].length;
		if (isWord(words[i]) && count >= charIndex) {
			return words[i - 1];
		}
	}
	return null;
}


const hasPunctuation = (pair, words) => {
	const startWord = words[pair.start];
	const endWord = words[pair.end - 1];
	for (let i = 0; i < punctuations.length; i++) {
		if (hasCharAtEnd(startWord, punctuations[i]) || hasCharAtStart(endWord, punctuations[i])) {
			return true;
		}
	}
	return false;
};


const autocorrectPunctuations = (text, newChar) => {
	const buildLines = [];
	const lines = text.split(/\r?\n/);
	for (let j = 0; j < lines.length; j++) {
		const line = lines[j];
		const words = line.split(' ');
		if (words.length > 1) {
			const build = [];
			const pairs = getWordPairs(words);
			if (pairs.length === 0) {
				const validWords = getValidWords(words);
				build.push(validWords.join(' '));
			}
			else {
				for (let i = 0; i < pairs.length; i++) {
					const pair = pairs[i];
					let startWord = words[pair.start];
					let endWord = words[pair.end - 1];
					if (hasCharInPair(pair, words, newChar)) {
						startWord = trimPunctuations(startWord, true) + newChar;
						endWord = trimPunctuations(endWord, false);
						if (newChar !== ',') {
							endWord = endWord.charAt(0).toUpperCase() + endWord.substring(1);
						}
						else if (newChar === ',' && !hasMultipleCaps(endWord)) {
							endWord = endWord.charAt(0).toLowerCase() + endWord.substring(1)
						}
					}
					build.push(startWord);
					words[pair.end - 1] = endWord;
					if (i === pairs.length - 1) {
						build.push(endWord);
					}
				}
			}
			buildLines.push(build.join(' '));
		}
		else {
			buildLines.push(line);
		}
	}
	return buildLines.join('\n');
};


const hasMultipleCaps = (text) => {
	let count = 0;
	for (let i = 0; i < text.length; i++) {
		if (isAlpha(text[i]) && text[i] === text[i].toUpperCase()) {
			count++;
		}
	}
	return text.length == 1 || count > 1;
};


const hasCharInPair = (pair, words, char) => {
	const startWord = words[pair.start];
	const endWord = words[pair.end - 1];
	if (hasCharAtEnd(startWord, char)) {
		return true;
	}
	if (hasCharAtStart(endWord, char)) {
		return true;
	}
	for (let i = pair.start + 1; i < pair.end - 1; i++) {
		if (words[i].includes(char)) {
			return true;
		}
	}
	return false;
}


const hasCharAtEnd = (word, char) => {
	for (let i = word.length - 1; i >= 0; i--) {
		if (isAlphaNum(word[i])) {
			return false;
		}
		if (word[i] === char) {
			return true;
		}
	}
};


const hasCharAtStart = (word, char) => {
	for (let i = 0; i < word.length; i++) {
		if (isAlphaNum(word[i])) {
			return false;
		}
		if (word[i] === char) {
			return true;
		}
	}
};


const isAlpha = (char) => {
	return char.match(/[a-z]/i) !== null;
}


const isAlphaNum = (char) => {
	return char.match(/^[a-z0-9]+$/i) !== null;
};


const trimPunctuations = (text, fromEnd) => {
	let build = '';
	let startIndex = 0;
	let endIndex = text.length - 1;
	if (!fromEnd) {
		for (let i = 0; i < text.length; i++) {
			if (!punctuations.includes(text[i])) {
				startIndex = i;
				break;
			}
		}
	}
	else {
		for (let i = text.length - 1; i >= 0; i--) {
			if (!punctuations.includes(text[i])) {
				endIndex = i;
				break;
			}
		}
	}
	return text.slice(startIndex, endIndex + 1);
};


const isWord = (text) => {
	let hasAlphaNum = false;
	for (let i = 0; i < text.length; i++) {
		if (text[i] === ' ') {
			return false;
		}
		if (isAlphaNum(text[i])) {
			hasAlphaNum = true;
		}
	}
	return hasAlphaNum;
};


const getValidWords = (words) => {
	const valids = [];
	for (let i = 0; i < words.length; i++) {
		if (isWord(words[i])) {
			valids.push(words[i]);
		}
	}
	return valids;
}


const getWordPairs = (words) => {
	const pairs = [];
	let start = 0;;
	let end = 0;
	let startSet = false;
	for (let i = 0; i < words.length; i++) {
		if (isWord(words[i]) && !startSet) {
			start = i;
			startSet = true;
		}
		else if (isWord(words[i]) && startSet) {
			end = i + 1;
			pairs.push({
				start: start,
				end: end
			});
			start = i;
		}
	}
	return pairs;
}


const isAtStartOfAWord = (index, text) => {
	if (index - 2 > 0 && text.charAt(index - 2) === ' ') {
		return true;
	}
	else {
		return false;
	}
}
