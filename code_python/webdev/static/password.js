const email = document.getElementById('input-email');
const password = document.getElementById('input-password');
const passwordConfirm = document.getElementById('input-password-confirm');
const passwordStrengthMeter = document.getElementById('password-strength-meter');

const widths = {
	0: 0,
	1: 25,
	2: 50,
	3: 75,
	4: 100
};

const colors = {
	0: 'bg-danger',
	1: 'bg-danger',
	2: 'bg-warning',
	3: 'bg-success',
	4: 'bg-success'
};

const resetPasswordStrengthMeter = () => {
	for (const score in colors) {
		passwordStrengthMeter.classList.remove(colors[score]);
	}
}

const confirmPassword = () => {
	if (password.value != passwordConfirm.value) {
		passwordConfirm.setCustomValidity('Passwords do not match.');
	} else {
		passwordConfirm.setCustomValidity('');
	}
}

document.addEventListener('keyup', () => {

	const passwordStrength = zxcvbn(password.value, user_inputs=[email.value]);

	resetPasswordStrengthMeter();

	if (password.value != '') {
		passwordStrengthMeter.style.width = widths[passwordStrength.score] + '%';
		passwordStrengthMeter.classList.add(colors[passwordStrength.score]);
	} else {
		passwordStrengthMeter.style.width = '0%';
		passwordStrengthMeter.classList.add(colors[0]);
	}

	if (passwordStrength.score < 3) {
		password.setCustomValidity('Password must be in the green.');
	} else {
		password.setCustomValidity('');
	}

	confirmPassword();
});
