const navNotes = document.getElementById('nav-notes');
const btnRecord = document.getElementById('btn-record');
const btnSubmit = document.getElementById('btn-submit');
const btnRecordIcon = document.getElementById('btn-record-icon');
const recordingIcon = document.getElementById('recording-icon');
const submitIcon = document.getElementById('submit-icon');
const player = document.getElementById('player');
const people = document.getElementById('people');
const pronoun = document.getElementById('pronoun');
const audioURL = document.getElementById('audio-url');
const notesURL = document.getElementById('notes-url');
const csrfToken = document.getElementById('csrf_token');
const inputName = document.getElementById('input-name');
const inputAudioFile = document.getElementById('input-audio-file');
const inputCaseFile = document.getElementById('input-case-file');
const showCaseFile = document.getElementById('show-case-file');
const btnAudioFile = document.getElementById('btn-audio-file');
const btnTrashAudio = document.getElementById('btn-trash-audio');
const btnCaseFile = document.getElementById('btn-case-file');
const btnTrashCase = document.getElementById('btn-trash-case');
const inputCall = document.getElementById('input-call');
const btnCall = document.getElementById('btn-call');
const callingIcon = document.getElementById('calling-icon');
const callPlayer = document.getElementById('call-player');
const playerLocal = document.getElementById('player-local');
const playerRemote = document.getElementById('player-remote');

navNotes.classList.add('active');

const AudioContext = window.AudioContext || window.webkitAudioContext;

let audioContext = null;
let input = null;
let recorder = null;

const showIsRecording = () => {
	btnRecordIcon.classList.remove('fa-circle');
	btnRecordIcon.classList.add('fa-pause');
	recordingIcon.style.visibility = 'visible';
}

const hideIsRecording = () => {
	btnRecordIcon.classList.remove('fa-pause');
	btnRecordIcon.classList.add('fa-circle');
	recordingIcon.style.visibility = 'hidden';
}

const handleSuccess = (stream) => {
	input = audioContext.createMediaStreamSource(stream);
	recorder = new Recorder(input, { numChannels: 1 });
	recorder.record();
	showIsRecording();
}

btnRecord.addEventListener('click', () => {
	window.onbeforeunload = () => {
		return true;
	};
	if (audioContext === null) {
		audioContext = new AudioContext();
	}
	if (recorder === null) {
		navigator.mediaDevices.getUserMedia({ audio: true }).then(handleSuccess);
	}
	else if (recorder.recording) {
		recorder.stop();
		recorder.exportWAV((audio) => {
			const url = URL.createObjectURL(audio);
			player.src = url;
		});
		hideIsRecording();
		btnSubmit.disabled = false;
		btnTrashAudio.disabled = false;
	}
	else {
		recorder.record();
		showIsRecording();
		btnSubmit.disabled = true;
		btnTrashAudio.disabled = true;
	}
	btnAudioFile.disabled = true;
	btnCall.disabled = true;
});

const submitNote = (audio) => {
	const xhr = new XMLHttpRequest();
	xhr.onload = () => {
		if (xhr.readyState === 4) {
			submitIcon.style.visibility = 'hidden';
			window.location.href = notesURL.content;
		}
	};
	const formData = new FormData();
	const peopleValue = people.options[people.selectedIndex].value;
	const pronounValue = pronoun.options[pronoun.selectedIndex].value;
	formData.append('audio', audio, 'audio.wav');
	formData.append('people', peopleValue);
	formData.append('pronoun', pronounValue);
	formData.append('input-name', inputName.value);
	formData.append('encounter-type', 'in-person');
	if (inputCaseFile.files.length === 1) {
		formData.append('case-template', inputCaseFile.files[0], 'case-template.json')
	}
	xhr.open('POST', audioURL.content, true);
	xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
	xhr.send(formData);
};

btnAudioFile.addEventListener('click', () => {
	inputAudioFile.click();
});

inputAudioFile.addEventListener('change', () => {
	window.onbeforeunload = () => {
		return true;
	};
	const audio = inputAudioFile.files[0];
	const url = URL.createObjectURL(audio);
	player.src = url;
	btnRecord.disabled = true;
	btnSubmit.disabled = false;
	btnTrashAudio.disabled = false;
	btnCall.disabled = true;
});

btnTrashAudio.addEventListener('click', () => {
	const message = 'Are you sure? Recording will not be saved.'
	if (!btnRecord.disabled && !confirm(message)) {
		return;
	}
	window.onbeforeunload = null;
	player.src = '';
	if (recorder !== null) {
		recorder.clear();
	}
	inputAudioFile.value = '';
	btnRecord.disabled = false;
	btnAudioFile.disabled = false;
	btnTrashAudio.disabled = true;
	btnSubmit.disabled = true;
});

btnCaseFile.addEventListener('click', () => {
	inputCaseFile.click();
});

inputCaseFile.addEventListener('change', () => {
	btnCaseFile.disabled = true;
	btnTrashCase.disabled = false;
	showCaseFile.value = inputCaseFile.files[0].name;
});

btnTrashCase.addEventListener('click', () => {
	btnCaseFile.disabled = false;
	btnTrashCase.disabled = true;
	inputCaseFile.value = '';
	showCaseFile.value = '';
});

const getCaseFile = () => {
	if (inputCaseFile.files.length === 1) {
		return inputCaseFile.files[0];
	}
};

let peer = null;
let conn = null;
let call = null;
let calling = false;
let localAudioContext = null;
let localStreamSource = null;
let localAudioRecorder = null;
let localAudio = null;
let remoteAudioContext = null;
let remoteStreamSource = null;
let remoteAudioRecorder = null;
let remoteAudio = null;

const mergeLocalAndRemoteAudio = new Promise((localAudioRecorder, remoteAudioRecorder) => {
	localAudioRecorder.export
});

btnCall.addEventListener('click', () => {
	btnRecord.disabled = true;
	btnTrashAudio.disabled = true;
	btnAudioFile.disabled = true;
	if (!calling) {
		const receiverId = inputCall.value;
		peer = new Peer({ 
			host: 'digitalscribe.xyz', 
			port: 9002, 
			path: '/', 
			secure: true });
		conn = peer.connect(receiverId);
		navigator.mediaDevices.getUserMedia({ audio: true }).then(localStream => {
			call = peer.call(receiverId, localStream);
			call.on('stream', remoteStream => {

				localAudioContext = new AudioContext();
				localStreamSource = localAudioContext.createMediaStreamSource(localStream);
				localAudioRecorder = new Recorder(localStreamSource, { numChannels: 1 });

				remoteAudioContext = new AudioContext();
				remoteStreamSource = remoteAudioContext.createMediaStreamSource(remoteStream);
				remoteAudioRecorder = new Recorder(remoteStreamSource, { numChannels: 1 });

				localAudioRecorder.record();
				remoteAudioRecorder.record();

				callPlayer.srcObject = remoteStream;
				callPlayer.play();
				callingIcon.style.visibility = 'visible';
				btnCall.classList.remove('btn-success');
				btnCall.classList.add('btn-danger');
				calling = true;
			});
			call.on('close', () => {
				callingIcon.style.visibility = 'hidden';
				btnCall.classList.remove('btn-danger');
				btnCall.classList.add('btn-success');
				localAudioRecorder.exportWAV(localAudio => {
					remoteAudioRecorder.exportWAV(remoteAudio => {
						const localAudioURL = URL.createObjectURL(localAudio);
						const remoteAudioURL = URL.createObjectURL(remoteAudio);
						playerLocal.src = localAudioURL;
						playerRemote.src = remoteAudioURL;
					});
				});
				calling = false;
				btnSubmit.disabled = false;
			});
		});
	} else if (call !== null && localAudioRecorder !== null && remoteAudioRecorder !== null) {
		call.close();
		conn.close();
		peer.destroy();
	}
});

const submitTelemedicineNote = (localAudioRecorder, remoteAudioRecorder) => {
	localAudioRecorder.exportWAV((localAudio) => {
		remoteAudioRecorder.exportWAV((remoteAudio) => {
			const xhr = new XMLHttpRequest();
			xhr.onload = () => {
				if (xhr.readyState === 4) {
					submitIcon.style.visibility = 'hidden';
					window.location.href = notesURL.content;
				}
			};
			const formData = new FormData();
			const peopleValue = people.options[people.selectedIndex].value;
			const pronounValue = pronoun.options[pronoun.selectedIndex].value;
			formData.append('local-audio', localAudio, 'local-audio.wav');
			formData.append('remote-audio', remoteAudio, 'remote-audio.wav');
			formData.append('people', peopleValue);
			formData.append('pronoun', pronounValue);
			formData.append('input-name', inputName.value);
			formData.append('encounter-type', 'telemedicine');
			if (inputCaseFile.files.length === 1) {
				formData.append('case-template', inputCaseFile.files[0], 'case-template.json')
			}
			xhr.open('POST', audioURL.content, true);
			xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
			xhr.send(formData);
		});
	});
}

btnSubmit.addEventListener('click', () => {

	window.onbeforeunload = null;

	const inputNameValue = inputName.value.trim();

	if (inputNameValue === '') {
		const message = "Submit without session name?";
		if (!confirm(message)) {
			return;
		}
	}

	submitIcon.style.visibility = 'visible';
	btnSubmit.disabled = true;

	if (localAudioRecorder !== null && remoteAudioRecorder !== null) {
		submitTelemedicineNote(localAudioRecorder, remoteAudioRecorder);
	}
	else if (inputAudioFile.files.length == 0 && recorder !== null) {
		recorder.exportWAV((audio) => {
			submitNote(audio);
		});
	} else {
		const audio = inputAudioFile.files[0];
		submitNote(audio);
	}
});
