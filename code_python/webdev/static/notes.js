const navNotes = document.getElementById('nav-notes');
const main = document.getElementById('main');
const metasDateUTC = document.getElementsByClassName('datetime-utc');
const linksTrash = document.getElementsByClassName('link-trash');
const clickableRowParts = document.getElementsByClassName('clickable-row-part');
const statusIcons = document.getElementsByClassName('status-icon');
const csrfToken = document.getElementById('csrf_token');
const trashURL = document.getElementById('trash-url');
const refreshNotesURL = document.getElementById('refresh-notes-url');
const globalCheckbox = document.getElementById('global-checkbox');
const noteCheckboxes = document.getElementsByClassName('note-checkbox');
const btnTrashGlobal = document.getElementById('btn-trash-global');
const formCheckInputs = document.getElementsByClassName('form-check-input');

navNotes.classList.add('active');

const getNoteID = (elem) => {
	return elem.id.split('-')[0];
};

for (let i = 0; i < metasDateUTC.length; i++) {
	const elem = metasDateUTC[i];
	const noteID = getNoteID(elem);
	const date = new Date(elem.content);
	const dateLocal = document.getElementById(noteID + '-date-local');
	const timeLocal = document.getElementById(noteID + '-time-local');
	dateLocal.innerHTML = ' ' + date.toLocaleDateString();
	timeLocal.innerHTML = ' ' + date.toLocaleTimeString(
		[], {hour: '2-digit', minute: '2-digit'});
}

for (let i = 0; i < linksTrash.length; i++) {
	const elem = linksTrash[i];
	const noteID = getNoteID(elem);
	elem.addEventListener('click', (e) => {
		e.preventDefault();
		if (confirm('Delete this note?')) {
			const xhr = new XMLHttpRequest();
			xhr.onload = () => {
				if (xhr.readyState === 4) {
					location.reload(true);
				}
			};
			const formData = new FormData();
			formData.append('note-ids', noteID);
			xhr.open('POST', trashURL.content, true);
			xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
			xhr.send(formData);
		}
	});
}

const refreshClickable = () => {
	for (let i = 0; i < clickableRowParts.length; i++) {
		const elem = clickableRowParts[i];
		const noteID = getNoteID(elem);
		const statusIcon = document.getElementById(noteID + '-status-icon');
		const status = statusIcon.getAttribute('data-status');
		if (status !== 'pending') {
			const clickURL = document.getElementById(noteID + '-click-url');
			elem.addEventListener('click', (e) => {
				window.location.href = clickURL.content;
			});
			elem.style.cursor = 'pointer';
		}
	}
};

refreshClickable();

for (let i = 0; i < statusIcons.length; i++) {
	const elem = statusIcons[i];
	const status = elem.getAttribute('data-status');
	if (status === 'pending') {
		elem.classList.add('fa-spinner');
	}
	else if (status === 'failed') {
		elem.classList.add('fa-exclamation');
	}
}

const getPendingNoteIDs = () => {
	const noteIDs = [];
	for (let i = 0; i < statusIcons.length; i++) {
		const elem = statusIcons[i];
		const noteID = getNoteID(elem);
		const status = elem.getAttribute('data-status');
		if (status === 'pending') {
			noteIDs.push(noteID);
		}
	}
	return noteIDs;
};

const refreshPendingNotes = () => {
	const pendingNoteIDs = getPendingNoteIDs();
	if (pendingNoteIDs.length > 0) {
		const xhr = new XMLHttpRequest();
		xhr.onload = () => {
			if (xhr.readyState === 4) {
				const data = JSON.parse(xhr.response);
				for (let i = 0; i < data.length; i++) {
					const noteID = data[i]._id;
					const status = data[i].status;
					const elem = document.getElementById(noteID + '-status-icon');
					if (elem !== null) {
						elem.setAttribute('data-status', status)
						elem.classList.remove('fa-spinner');
						elem.classList.remove('fa-exclamation');
						if (status === 'pending') {
							elem.classList.add('fa-spinner');
						}
						else if (status === 'failed') {
							elem.classList.add('fa-exclamation');
						}
					}
				}
				refreshClickable();
			}
		};
		const formData = new FormData();
		formData.append('note-ids', pendingNoteIDs.join());
		xhr.open('POST', refreshNotesURL.content, true);
		xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
		xhr.send(formData);
	}
};

setInterval(refreshPendingNotes, 5000);

if (globalCheckbox !== null) {
	globalCheckbox.addEventListener('click', () => {
		for (let i = 0; i < noteCheckboxes.length; i++) {
			noteCheckboxes[i].checked = globalCheckbox.checked;
		}
	});
}

const hasCheckedNote = () => {
	for (let i = 0; i < noteCheckboxes.length; i++) {
		if (noteCheckboxes[i].checked) {
			return true;
		}
	}
	return false;
};

for (let i = 0; i < formCheckInputs.length; i++) {
	formCheckInputs[i].addEventListener('click', () => {
		if (hasCheckedNote() || globalCheckbox.checked) {
			btnTrashGlobal.hidden = false;
		}
		else {
			btnTrashGlobal.hidden = true;
		}
	});
}

const getCheckedNoteIDs = () => {
	const noteIDs = [];
	for (let i = 0; i < noteCheckboxes.length; i++) {
		const elem = noteCheckboxes[i];
		const noteID = getNoteID(elem);
		if (elem.checked) {
			noteIDs.push(noteID);
		}
	}
	return noteIDs;
};

btnTrashGlobal.addEventListener('click', () => {
	const noteIDs = getCheckedNoteIDs();
	if (confirm(`Delete ${noteIDs.length} note(s)?`)) {
		const xhr = new XMLHttpRequest();
		xhr.onload = () => {
			if (xhr.readyState === 4) {
				location.reload(true);
			}
		};
		const formData = new FormData();
		formData.append('note-ids', noteIDs.join('|'));
		xhr.open('POST', trashURL.content, true);
		xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
		xhr.send(formData);
	}
});

const checkNotes = (initIndex, stopIndex) => {
	for (let i = 0; i < noteCheckboxes.length; i++) {
		const elem = noteCheckboxes[i];
		if (i >= initIndex && i <= stopIndex) {
			elem.checked = true;
		}
	}
};

let lastCheckedIndex = null;

for (let i = 0; i < noteCheckboxes.length; i++) {
	const elem = noteCheckboxes[i];
	const elemIndex = elem.getAttribute('data-index');
	elem.addEventListener('click', event => {
		if (elem.checked) {
			if (lastCheckedIndex !== null && event.shiftKey) {
				const initIndex = Math.min(elemIndex, lastCheckedIndex);
				const stopIndex = Math.max(elemIndex, lastCheckedIndex);
				checkNotes(initIndex, stopIndex);
			}
			lastCheckedIndex = elemIndex;
		}
		else if (!hasCheckedNote()) {
			lastCheckedIndex = null;
			globalCheckbox.checked = false;
			btnTrashGlobal.hidden = true;
		}
	});
}

main.style.visibility = 'visible';
