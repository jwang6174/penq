// Declare HTML elements.
const main = document.getElementById('main');
const noteID = document.getElementById('note-id');
const csrfToken = document.getElementById('csrf_token');
const saveURL = document.getElementById('save-url');
const saveBtn = document.getElementById('btn-save');
const saveIcon = document.getElementById('icon-save');
const navNotes = document.getElementById('nav-notes');
const section = document.getElementById('section');
const stopwatchURL = document.getElementById('stopwatch-url');
const btnSub = document.getElementById('btn-sub');
const btnObj = document.getElementById('btn-obj');
const btnAnp = document.getElementById('btn-anp');
const sectionItems = document.getElementsByClassName('section-item');
const btnDictate = document.getElementById('btn-dictate');
const recordingIcon = document.getElementById('recording-icon');
const dictationURL = document.getElementById('dictation-url');
const editableElem = document.getElementById('editable');
const btnLock = document.getElementById('btn-lock');
const lockURL = document.getElementById('lock-url');
const btnData = document.getElementById('btn-data');
const dataURL = document.getElementById('data-url');
const editors = document.getElementsByClassName('editor');


// Highlight the `notes` navbar button.
navNotes.classList.add('active');


// Highlight the `subjective` dropdown menu item.
btnSub.classList.add('active');


// Check if note is editable on default and disable elements
// accordingly.
let isEditable = (editableElem.content.toLowerCase() === 'true');
if (!isEditable) {
	saveBtn.disabled = true;
	btnDictate.disabled = true;
	btnLock.disabled = true;
}


// Reference current text area.
let currentEditor = null;


// Used to determine whether the note has changed and needs to be
// saved.
let snapshot = null;


// Used to determine when to automatically save the note.
let timeout = null;


// True if autocorrect is enabled, else false.
let autocorrectEnabled = true;


// True if dictation is enabled, else false.
let dictationEnabled = false;


// Check which part of the note is being edited.
const getActiveSection = () => {
	if (btnSub.classList.contains('active')) {
		return 'sub';
	}
	else if (btnObj.classList.contains('active')) {
		return 'obj';
	}
	else if (btnAnp.classList.contains('active')) {
		return 'anp';
	}
};


// De-highlight all section dropdown menu items.
const deactivateSectionItems = () => {
	for (let i = 0; i < sectionItems.length; i++) {
		sectionItems[i].classList.remove('active');
	}
};


// Hide all text areas.
const hideAllEditors = () => {
	for (let i = 0; i < editors.length; i++) {
		editors[i].style.display = 'none';
	}
};


// Change window to selected note section.
const setActiveSection = (selected) => {
	deactivateSectionItems();
	hideAllEditors();
	selected.classList.add('active');
	const activeSection = getActiveSection();
	currentEditor = document.getElementById('editor-' + activeSection);
	currentEditor.style.display = 'inline';
	snapshot = currentEditor.value;
	if (!isEditable) {
		currentEditor.disabled = true;
	}
};


// Set default section to `subjective`
setActiveSection(btnSub);


// Save note to server
const save = () => {
	saveIcon.classList.remove('fa-save');
	saveIcon.classList.add('fa-spinner');
	const markupEdit = currentEditor.value;
	snapshot = markupEdit;
	const xhr = new XMLHttpRequest();
	xhr.onload = () => {
		if (xhr.readyState === 4) {
			setTimeout(() => {
				saveIcon.classList.remove('fa-spinner');
				saveIcon.classList.add('fa-save');
			}, 500);
		}
	};
	const formData = new FormData();
	formData.append('note-id', noteID.content);
	formData.append('markup', markupEdit);
	formData.append('section', getActiveSection());
	xhr.open('POST', saveURL.content, true);
	xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
	xhr.send(formData);
};


// Add behavior for when user clicks on a section dropdown menu item.
for (let i = 0; i < sectionItems.length; i++) {
	const elem = sectionItems[i];
	elem.addEventListener('click', () => {
		if (snapshot !== currentEditor.value) {
			save();
		}
		setActiveSection(elem);
	});
}


// Add behavior for when user clicks the save button.
saveBtn.addEventListener('click', () => {
	save();
});


// Add behavior for when user types.
document.addEventListener('keydown', (e) => {

	// When user hits Ctrl+S
	if ((e.ctrlKey || e.metaKey) && e.which === 83) {
		e.preventDefault();
		save();
	}

	// When user hits comma, period, backspace, or delete.
	else if (autocorrectEnabled && (e.which === 188 || e.which === 190 || e.which === 8 || e.which === 46 || e.which === 32)) {
		setTimeout(() => {
			let cursorIndex = 0;
			if (e.which === 188 || e.which === 190) {
				if (isAtStartOfAWord(currentEditor.selectionStart, currentEditor.value)) {
					cursorIndex = currentEditor.selectionStart - 1;
				}
				else {
					cursorIndex = currentEditor.selectionStart;
				}		
			}
			else {
				cursorIndex = currentEditor.selectionStart;
			}
			if (e.which === 188) {
				currentEditor.value = autocorrectPunctuations(currentEditor.value, ',');
			}
			else if (e.which === 190) {
				currentEditor.value = autocorrectPunctuations(currentEditor.value, '.');
			}
			else if (e.which === 8 || e.which === 46 || e.which === 32) {
				currentEditor.value = autocorrectCapitalizations(currentEditor.value);
			}
			currentEditor.selectionStart = cursorIndex;
			currentEditor.selectionEnd = cursorIndex;
		}, 0);
	}

	// Autosave note.
	timeout = setTimeout(() => {
		if (snapshot !== currentEditor.value) {
			save();
		}
	}, 1000);
});


// Show window after rendering.
main.style.visibility = 'visible';


// Track time user spends on the current note section in 1-second
// increments.
setInterval(() => {
	if (isEditable) {
		const xhr = new XMLHttpRequest();
		const formData = new FormData();
		formData.append('note-id', noteID.content);
		formData.append('section', getActiveSection());
		formData.append('interval', 1);
		xhr.open('POST', stopwatchURL.content, true);
		xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
		xhr.send(formData);
	}
}, 1000);


// Variables for dictation.
let final_transcript = '';
let recognizing = false;
let ignore_onend;
let start_timestamp;
const recognition = new webkitSpeechRecognition();


// Set dictation settings.
recognition.continuous = true;
recognition.interimResults = true;


// Store recognition state.
recognition.onstart = () => {
	recognizing = true;
	console.log('info_speak_now');
};


// Handle speech recognition errors.
recognition.onerror = (event) => {
	if (event.error === 'no-speech') {
		console.log('info_no_speech');
		ignore_onend = true;
	}
	if (event.error === 'audio-capture') {
		console.log('info_no_microphone');
		ignore_onend = true;
	}
	if (event.error === 'not-allowed') {
		if (event.timeStamp - start_timestamp < 100) {
			console.log('info_blocked');
		} else {
			console.log('info_denied');
		}
		ignore_onend = true;
	}
};


// Add behavior for when speech recognition stops.
recognition.onend = () => {
	recognizing = false;
	if (ignore_onend) {
		return;
	}
	if (!final_transcript) {
		console.log('info_start');
		return;
	}
};


// Capitalize string.
const capitalize = (s) => {
	const sentences = s.split('. ');
	for (let i = 0; i < sentences.length; i++) {
		sentences[i] = sentences[i].replace(/\S/, function(m) { return m.toUpperCase(); });
	}
	return sentences.join('. ');
};


// Save dictation to server.
const saveTranscript = (transcript) => {
	const xhr = new XMLHttpRequest();
	const formData = new FormData();
	formData.append('note-id', noteID.content);
	formData.append('section', getActiveSection());
	formData.append('transcript', transcript);
	xhr.open('POST', dictationURL.content, true);
	xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
	xhr.send(formData);
};


// When speech recognition returns a result and editor is in focus,
// add the dictation to the editor.
recognition.onresult = (event) => {
	for (let i = event.resultIndex; i < event.results.length; i++) {
		if (event.results[i].isFinal && currentEditor === document.activeElement) {
			const transcript = capitalize(event.results[i][0].transcript);
			if (final_transcript === ' ') {
				final_transcript += transcript;
			} else {
				final_transcript = transcript;
			}
			const start = currentEditor.selectionStart;
			const end = currentEditor.selectionEnd;
			currentEditor.value = currentEditor.value.slice(0, start) + final_transcript + currentEditor.value.slice(end);
			currentEditor.selectionStart = currentEditor.selectionEnd = start + final_transcript.length;
			saveTranscript(final_transcript);
			save();
		}
	}
};


// Start speech recognition when user clicks on the dictation button.
btnDictate.addEventListener('click', () => {
	if (recognizing) {
		dictationEnabled = false;
		autocorrectEnabled = true;
		recognition.stop();
		recordingIcon.style.visibility = 'hidden';
		return;
	}
	dictationEnabled = true;
	autocorrectEnabled = false;
	final_transcript = '';
	recognition.start();
	recordingIcon.style.visibility = 'visible';
});


// Periodically re-start the Web Speech API if `dictationEnabled` is true.
setInterval(() => {
	if (dictationEnabled && !recognizing) {
		recognition.start();
		final_transcript = ' ';
		console.log('auto-restart dictation')
	}
}, 500);


// Download note data file.
btnData.addEventListener('click', () => {
	const a = document.createElement('a');
	a.download = 'note-data.json';
	a.href = dataURL.content;
	a.click();
});


// Lock note to prevent edits and stop the timer.
btnLock.addEventListener('click', () => {
	const message = "Are you sure? Locking will prevent future editing.";
	if (!confirm(message)) {
		return;
	}
	save();
	const xhr = new XMLHttpRequest();
	const formData = new FormData();
	formData.append('note-id', noteID.content);
	xhr.open('POST', lockURL.content, true);
	xhr.setRequestHeader('X-CSRFToken', csrfToken.content);
	xhr.send(formData);
	isEditable = false;
	currentEditor.disabled = true;
	saveBtn.disabled = true;
	btnDictate.disabled = true;
	btnLock.disabled = true;
	if (recognizing) {
		recognition.stop();
		recordingIcon.style.visibility = 'hidden';
	}
});


// Create radial context menu for editing punctuations.
const punctuationContextMenu = new RadialMenu({
	fontSize: 14,
	innerCircle: 10,
	outerCircle: 90,
	rotation: 0,
	shadowColor: 'rgba(0, 0, 0, 0.2)',
	shadowBlur: 10,
	shadowOffsetX: 3,
	shadowOffsetY: 3,
	backgroundColor: '#EEE',
	borderColor: '#FFF',
	textColor: '#000',
	textBorderColor: 'transparent',
	textShadowColor: 'transparent',
	textShadowBlur: 10,
	textShadowOffsetX: 3,
	textShadowOffsetY: 3,
	buttons: [
		{'text': 'Comma', 'action': () => editPunctuation(currentEditor, 'comma') },
		{'text': 'Delete', 'action': () => editPunctuation(currentEditor, 'delete') },
		{'text': 'Period', 'action': () => editPunctuation(currentEditor, 'period') }
	]
});
