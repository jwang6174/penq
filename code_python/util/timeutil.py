'''
Help with time-related tasks.
'''
from time import strftime


def timelog(message):
    '''
    Args:
    (str) message -- Display time with this message.
    '''
    print('%s | %s' % (strftime('%c'), message))
