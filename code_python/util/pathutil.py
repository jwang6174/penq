'''
Help with data-related tasks.
'''

import os

# Test directory.
TEST_DIR = os.path.join('/', 'home', 'jessewang', 'penq', 'code_python', 'test')

# Data directory.
DATA_DIR = os.path.join('/', 'home', 'jessewang', 'data', 'penq')

# Logs directory.
LOGS_DIR = os.path.join(DATA_DIR, 'logs')
os.makedirs(LOGS_DIR, exist_ok=True)

# Models directory.
MODELS_DIR = os.path.join(DATA_DIR, 'models')
os.makedirs(MODELS_DIR, exist_ok=True)

# Database dumps.
DUMPS_DIR = os.path.join(DATA_DIR, 'dumps')
os.makedirs(DUMPS_DIR, exist_ok=True)

# Keys directory.
KEYS_DIR = os.path.join(DATA_DIR, 'keys')
os.makedirs(KEYS_DIR, exist_ok=True)

# Configuration directory.
CONF_DIR = os.path.join(DATA_DIR, 'conf')
os.makedirs(CONF_DIR, exist_ok=True)

# Audio directory.
AUDIO_DIR = os.path.join(DATA_DIR, 'audio')
os.makedirs(AUDIO_DIR, exist_ok=True)

# MeSH directory.
MESH_DIR = os.path.join(DATA_DIR, 'mesh')
os.makedirs(MESH_DIR, exist_ok=True)

# Stanford NLP directory.
STANFORD_NLP_DIR = os.path.join(DATA_DIR, 'stanford-corenlp-full-2018-10-05')
os.makedirs(STANFORD_NLP_DIR, exist_ok=True)
