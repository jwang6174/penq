'''
Help with database-related tasks.
'''

from pymongo import MongoClient

# Mongo host IP address.
MONGO_HOST = '127.0.0.1'

# Mongo client.
MONGO_CLIENT = MongoClient(MONGO_HOST)
