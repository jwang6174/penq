'''
Help with audio-related tasks.
'''

from pydub.utils import mediainfo


def get_sample_rate(filepath):
    '''
    Args:
    (str) filepath -- Audio filepath.

    Returns:
    (int) Audio sample rate.
    '''
    return mediainfo(filepath)['sample_rate']
