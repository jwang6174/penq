## Notes
* To avoid 413 Request Entity Too Large error when uploading audio, follow this guide: https://www.keycdn.com/support/413-request-entity-too-large.
* Calculate size of audio file: https://www.colincrawley.com/audio-file-size-calculator/