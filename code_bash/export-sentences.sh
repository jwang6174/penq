mongoexport --db=penq --collection=sentences.medical.training --out=sentences_medical_training_part.json --jsonArray --limit=100000
mongoexport --db=penq --collection=sentences.medical.validation --out=sentences_medical_validation_part.json --jsonArray --limit=100000
mongoexport --db=penq --collection=sentences.medical.test --out=sentences_medical_test_part.json --jsonArray --limit=100000
mongoexport --db=penq --collection=sentences.everyday.training --out=sentences_everyday_training_part.json --jsonArray --limit=100000
mongoexport --db=penq --collection=sentences.everyday.validation --out=sentences_everyday_validation_part.json --jsonArray --limit=100000
mongoexport --db=penq --collection=sentences.everyday.test --out=sentences_everyday_test_part.json --jsonArray --limit=100000

mongoexport --db=penq --collection=sentences.medical.training --out=sentences_medical_training.json --jsonArray
mongoexport --db=penq --collection=sentences.medical.validation --out=sentences_medical_validation.json --jsonArray
mongoexport --db=penq --collection=sentences.medical.test --out=sentences_medical_test.json --jsonArray
mongoexport --db=penq --collection=sentences.everyday.training --out=sentences_everyday_training.json --jsonArray
mongoexport --db=penq --collection=sentences.everyday.validation --out=sentences_everyday_validation.json --jsonArray
mongoexport --db=penq --collection=sentences.everyday.test --out=sentences_everyday_test.json --jsonArray