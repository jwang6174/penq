cd ~/peerjs
nohup npx peerjs --port 9002 --sslkey /etc/letsencrypt/live/digitalscribe.xyz/privkey.pem --sslcert /etc/letsencrypt/live/digitalscribe.xyz/cert.pem &
