//
//  Organization.swift
//  Digital Scribe
//
//  Created by Jesse Wang on 12/8/19.
//  Copyright © 2019 Jesse Wang. All rights reserved.
//

struct Organization {
    
    let name: String
    let clientID: String
    let authorizeURL: String
    let tokenURL: String
    
}
