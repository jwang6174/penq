//
//  ViewController.swift
//  Digital Scribe
//
//  Created by Jesse Wang on 12/6/19.
//  Copyright © 2019 Jesse Wang. All rights reserved.
//

import UIKit
import SafariServices
import SearchTextField
import SwiftyJSON

class LoginViewController: UIViewController {
    
    @IBOutlet weak var organizationSearchField: SearchTextField!
    @IBOutlet weak var loginButton: UIButton!
                
    let organizations = [
        "Epic Sandbox": Organization(
            name: "Epic Sandbox",
            clientID: "31b1636f-e586-4a97-8045-91b1215b2449",
            authorizeURL: "https://apporchard.epic.com/interconnect-aocurprd-oauth/oauth2/authorize",
            tokenURL: "https://apporchard.epic.com/interconnect-aocurprd-oauth/oauth2/token")
    ]
        
    let redirectURI = "https://digitalscribe.ai"
            
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Set delegates
        organizationSearchField.delegate = self
        
        // Set navbar logo
        let logo = #imageLiteral(resourceName: "Logo")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        // Remove navbar border
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Set organization search field
        let organizationNames = Array(organizations.keys)
        organizationSearchField.filterStrings(organizationNames)
        organizationSearchField.theme.font = UIFont.systemFont(ofSize: 14)
        organizationSearchField.maxNumberOfResults = 5
        organizationSearchField.comparisonOptions = [.caseInsensitive]
        organizationSearchField.itemSelectionHandler = {items, itemPosition in
            self.organizationSearchField.text = items[itemPosition].title
            self.organizationSearchField.resignFirstResponder()
            self.showLoginButton()
        }
    }
                
    func hideLoginButton() {
        loginButton.alpha = 0
        loginButton.isEnabled = false
    }
    
    func showLoginButton() {
        loginButton.alpha = 1
        loginButton.isEnabled = true
    }
    
    func showLoginScreen(for organization: Organization) {
        
        // URL encode redirect URL
        guard let encodedRedirectURI = redirectURI.addingPercentEncoding(
            withAllowedCharacters: .urlQueryAllowed) else { return }
        
        // URL encode client ID
        guard let encodedClientID = organization.clientID.addingPercentEncoding(
            withAllowedCharacters: .urlQueryAllowed) else { return }

        // Build URL to request authorization code
        var urlString = organization.authorizeURL + "?"
        urlString += "response_type=code"
        urlString += "&redirect_uri=\(encodedRedirectURI)"
        urlString += "&client_id=\(encodedClientID)"
        guard let url = URL(string: urlString) else { return }

        // Open embedded Safari viewer for user to enter login credentials
        let configuration = SFSafariViewController.Configuration()
        configuration.entersReaderIfAvailable = true
        let viewController = SFSafariViewController(url: url, configuration: configuration)
        present(viewController, animated: true)
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {        
        guard let organizationName = organizationSearchField.text else { return }
        guard let organization = organizations[organizationName] else { return }
        showLoginScreen(for: organization)
    }
                    
}

// MARK: - UISearchTextFieldDelegate
extension LoginViewController: UISearchTextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hideLoginButton()
    }
    
}
